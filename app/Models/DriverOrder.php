<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DriverOrder extends Model
{

    protected $fillable = [
        'order_id',
        'driver_id',
        'latitute',
        'longitute',
        'address',
        'date'
    ];
    protected $hidden = [
        'created_at','updated_at'
    ];
    public function order() {

        return $this->belongsTo(Order::class);

    }

    public function user() {

        return $this->belongsTo(User::class , 'driver_id');

    }


    public function user_filter() {

        return $this->belongsTo(User::class , 'driver_id');

    }




}
