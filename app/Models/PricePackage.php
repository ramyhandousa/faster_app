<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricePackage extends Model
{
    protected $fillable = [ 'stage','price','kilometer'];
    
    protected $hidden = [
        'created_at','updated_at'
    ];


    public static function getPrice($key)
    {
        $option = PricePackage::where('id', $key)->first();
        return $option ? $option->price : null;
    }

    public static function getKilo($key)
    {
        $option = PricePackage::where('id', $key)->first();
        return $option ? $option->kilometer : null;
    }



}
