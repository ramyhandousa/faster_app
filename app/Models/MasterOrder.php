<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class MasterOrder extends Model
{
    protected $fillable =
        [
            'user_id',
            'provider_id',
            'driver_id',
            'status_provider',
            'status_driver',
            'cost',
            'services_discount',
            'percent_app',
        ];
        
     protected $appends = ['status_append'];


    public function orderDetails(){
        return $this->hasMany(OrderDetails::class);
    }
    
     public function provider() {
              return $this->belongsTo(User::class, 'provider_id');
       }
    
    
       public function driver() {
	    return $this->belongsTo(User::class, 'driver_id' );
       }

       public function driver_relation() {

	    return $this->hasOne(DriverOrder::class, 'order_id' );
       }
       
       
       public function getStatusAppendAttribute()
    {

        $status = $this->attributes['status_provider'];
        switch ($status) {

            case 'pending':
                return 'جاري تجهيز الطلب';
                break;

            case 'accepted':
                return 'تم الموافقة';
                break;

            case 'refuse':
                return 'تم رفض الطلب';
                break;

            default:
                return '';
        }
    }
}
