<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function user()
    {

        return $this->belongsTo(User::class);
    }
    
    
     public function sender()
    {

        return $this->belongsTo(User::class , 'sender_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
