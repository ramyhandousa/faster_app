<?php

namespace App\Models;

use App\Http\Resources\UserFilterRecource;
use App\Provider;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
       protected $fillable =
     [
        'user_id',
        'name',
        'phone',
        'city_id',
        'address',
        'latitute',
        'longitute',
        'kilometer',
        'shipping_cost',
        'status_provider',
        'status_driver',
        'message',

     ];
       
       public function user() {
              return $this->belongsTo(User::class);
       }

        
       
       public function city() {
	    return $this->belongsTo(City::class);
       }


       public function driver() {
	    return $this->hasOne(DriverOrder::class );
       }

       public function driver_relation() {

	    return $this->hasOne(DriverOrder::class );
       }




    // public function getStatusProviderAttribute()
    // {
    //     $status = $this->attributes['status_provider'];
    //     switch ($status) {

    //         case 'pending':
    //             return 'جاري تجهيز الطلب';
    //             break;

    //         case 'accepted':
    //             return 'تم الموافقة';
    //             break;

    //         case 'refuse':
    //             return 'تم رفض الطلب';
    //             break;

    //         default:
    //             return '';
    //     }
    // }

    // public function getStatusDriverAttribute()
    // {
    //     $status = $this->attributes['status_driver'];
    //     switch ($status) {

    //         case 'pending':
    //             return 'جاري تجهيز الطلب';
    //             break;

    //         case 'accepted':
    //             return 'تم الموافقة';
    //             break;

    //         case 'refuse':
    //             return 'تم رفض الطلب';
    //             break;

    //         case 'delivered':
    //             return 'تم الإنتهاء';
    //             break;

    //         default:
    //             return '';
    //     }
    // }
       
       
       
       
}
