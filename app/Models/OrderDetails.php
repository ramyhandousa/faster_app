<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{

    public function masterOrder(){
        return $this->belongsTo(MasterOrder::class);
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }


}
