<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MyAdminResetPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

//           if ($notifiable->roles->count() > 0) {


            $data = array(
                'url' => $url = url('/administrator/password/reset/' . $this->token),
                'title' => __('trans.reset_password'),
                'body' => __('trans.reset_body'),
                'logo' => request()->root() . "/public/assets/admin/images/logo.png"

            );
            return (new MailMessage)
                ->line('إعادة تعيين كلمة المرور')
                ->view('admin.emails.email', ['data' => $data]);
//                ->action('تغيير كلمة المرور', url('/administrator/password/reset/' . $this->token));

//        } else {
//            return (new MailMessage)
//                ->line('إعادة تعيين كلمة المرور')
//                ->action('تغيير كلمة المرور', url('/password/reset/' . $this->token));
//        }




    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
