<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null,$distance = null){

            $admins= Device::whereDeviceType('web')->pluck('user_id');
            $devicesWeb= Device::whereDeviceType('web')->pluck('device');
             $userIos= Device::where('user_id','!=',$sender)->whereDeviceType('Ios')->pluck('user_id');

            switch($type){

            case $type == 1:

                // contactUs Form admin
                //  $sender  admin who send
                // admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                  ];
                  $this->insertData($data);
             break;

            case $type == 2:

                     // contactUs Form Users
                  foreach ($admins as $admin){
                         $data = [
                           'user_id' => $admin ,
                           'sender_id' => $sender ,
                           'title' => trans('global.connect_us'),
                           'body' => $request ,
                           'type' => 2,
                         ];
                         $this->insertData($data);
                  }
                $this->push->sendPushNotification(null, $devicesWeb, 'تواصل معنا', $request);
                  break;

            case $type == 10:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'order_id' => $order,
                    'title' =>   'الطلبات'  ,
                    'body' => ' يوجد لديك طلب جديد قيمته ' . $additional ,
                    'value' => $additional ,
                    'distance' => $distance,
                    'type' => 10,
                ];
                $this->insertData($data);
             break;

            case $type == 11:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id,
                    'order_id' => $order,
                    'title' =>   'الطلبات'  ,
                    'body' =>      $order .'  تم قبول طلبك رقم  '.
                                    $sender->name.  '  من المندوب   ',
                    'type' => 11,
                ];
                $this->insertData($data);
             break;

            case $type == 12:

                 $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id,
                    'order_id' => $order,
                    'title' =>   'الطلبات'  ,
                    'body' =>      $order .'  بانهاء طلبك رقم  '.
                                    $sender->name.  '  قام المندوب   ',
                    'type' => 12,
                ];
                $this->insertData($data);

             break;

            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }

}
       
       