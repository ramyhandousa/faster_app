<?php

namespace App\Events;

use App\Company;
use App\Models\MasterOrder;
use App\Models\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotifyUsers
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $order;
    public $request;
    public $orders;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, MasterOrder $order,Request $request , $orders = [] )
    {
        $this->user = $user;
        $this->order = $order;
        $this->request = $request;
        $this->orders = $orders;

    }

}
