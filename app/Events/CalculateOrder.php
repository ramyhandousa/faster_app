<?php

namespace App\Events;

use App\Models\MasterOrder;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CalculateOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $order;
    public $orders;
    public $request;


    public function __construct(User $user,MasterOrder  $masterOrder, $orders, $request)
    {
        $this->user = $user;
        $this->order = $masterOrder;
        $this->orders = $orders;
        $this->request = $request;

    }


}
