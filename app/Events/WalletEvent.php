<?php

namespace App\Events;

use App\Models\MasterOrder;
use App\User;
use App\Wallet;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WalletEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $masterOrder;
    public $request;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, MasterOrder $masterOrder, $request)
    {
        $this->user = $user;
        $this->masterOrder = $masterOrder;
        $this->request = $request;

    }


}
