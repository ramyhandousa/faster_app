<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccptedOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $text;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order, $text , $message)
    {
        $this->order = $order;
        $this->text = $text;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.sendMails',compact('order','text','message'));
    }
}
