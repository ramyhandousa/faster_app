<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 11/16/2017
 * Time: 9:29 AM
 */

namespace App\Providers;

use App\Libraries\Main;
use App\Models\City;
use App\Models\PricePackage;
use App\Models\Support;
use App\User;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
 
            $helper = new \App\Http\Helpers\Images();
            $main_helper = new \App\Http\Helpers\Main();
            $setting = new Setting;
            $pricePakage = new PricePackage();
            $main = new Main();
            
            $providersCount = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->where('defined_user','provider')->count();

            $driversCount = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->where('defined_user','delivery')->count();

            $userHelpAdminCount = User::where('id', '!=', \Auth::id())->whereHas('roles', function ($q) {
                $q->where('name', '!=', 'owner');
            })->count();


            $citiesCount = City::count();


            $messageNotReadCount = Support::where('parent_id', 0)->whereIsRead(0)->count();

            $messageReadCount =  Support::where('parent_id', 0)->whereIsRead(1)->count();

           
           
           $view->with(
                compact('providersCount','pricePakage','driversCount','citiesCount','userHelpAdminCount','messageNotReadCount','messageReadCount',
                'helper', 'main', 'setting','main_helper','cities', 'branches')
            );
            
            
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


