<?php

namespace App\Providers;

use App\Events\AcceptedOrder;
use App\Events\AccepteOrRefuseDriverOrder;
use App\Events\AccepteOrRefuseProviderOrder;
use App\Events\CalculateOrder;
use App\Events\finishOrder;
use App\Events\NotifyUsers;
use App\Events\WalletEvent;
use App\Listeners\AcceptedOrderListener;
use App\Listeners\AccepteOrRefuseDriverOrderListener;
use App\Listeners\AccepteOrRefuseProviderOrderListener;
use App\Listeners\CalculateOrderListener;
use App\Listeners\finishOrderListener;
use App\Listeners\SendNotificationToUsers;
use App\Listeners\WalletListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NotifyUsers::class => [
            SendNotificationToUsers::class,
        ],
        AcceptedOrder::class => [
            AcceptedOrderListener::class,
        ],
        AccepteOrRefuseProviderOrder::class =>[
            AccepteOrRefuseProviderOrderListener::class,
        ],

         AccepteOrRefuseDriverOrder::class => [
            AccepteOrRefuseDriverOrderListener::class,
        ],
        finishOrder::class => [
            finishOrderListener::class,
        ],
       CalculateOrder::class => [
            CalculateOrderListener::class,
        ],
        WalletEvent::class => [
            WalletListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
