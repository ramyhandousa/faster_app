<?php

namespace App\Http\Middleware;

use App\Models\Order;
use App\User;
use Closure;

class providerAcceptedOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ( ! $user ) {   return $this->UserNotFound();  }


        if ($user['defined_user'] != 'provider'):  // check if not provider
            return $this->notProvider();
        endif;


        if (!$request->orders ||count($request->orders) < 0 ): // To make sure you have Orders
            return $this->checkForOrdersQuntatiy();
        endif;

        $orders = Order::whereIn('id', $request->orders)->get();

        $flage = $this->checkQuntatiy($orders);

        if ($flage['status'] == false){

            $order = Order::whereId($flage['error'][0]['order_id'])->first();

            return response()->json(['status' => 400 , 'message' =>  ' تم تغير حالته' . ' الطلب رقم  ' .$order->id ]);
        }

        return $next($request);
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function notProvider(){
        return response()->json([   'status' => 401,  'error' => (array) 'يجب التاكد انه مزود خدمة'   ],200);
    }

    private  function checkForOrdersQuntatiy(){
        return response()->json([   'status' => 400,  'error' => (array) 'يجب إختيار الطلبات المطلوب قبولها'   ],200);
    }


    private function checkQuntatiy($orders){

        $flage[] = true;
        $error = [];

        foreach ($orders as $item){

            $order = Order::whereId($item->id)->first();


            if ( $order['status_provider'] == 'accepted' || $order['status_provider'] == 'refuse' ){

                $flage[] = false;
                $error[] = [
                    'order_id' => $order->id
                ];
            }
        }

        if (in_array(false , $flage)){
            $data = [ 'status' => false , 'error' => $error];
            return $data;
        }

        $data = [  'status' => true ];
        return $data;
    }




}
