<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Countery;
use App\Models\Order;
use App\User;
use Closure;

class validOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $order = Order::whereId( $request->orderId)->first();

        if ( ! $order ) {   return $this->OrderNotFound();  }

        
        return $next($request);
    }
    
    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }


    private  function OrderUser(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب  لا ينتمي لمزود الخدمة'   ],200);
    }



}
