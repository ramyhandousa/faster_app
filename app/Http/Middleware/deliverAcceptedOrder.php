<?php

namespace App\Http\Middleware;

use App\Models\MasterOrder;
use App\Models\OrderDetails;
use App\Models\PricePackage;
use App\Models\Setting;
use App\Models\Order;
use App\User;
use Closure;

class deliverAcceptedOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->with('driver_orders')->first();

        if ( ! $user ) {   return $this->UserNotFound();  }


        if ($user['defined_user'] != 'delivery'):  // check if not provider
            return $this->notDelivery();
        endif;

        $checkOrder = MasterOrder::whereId($request->orderId)->first();

        if (!$checkOrder){
            return $this->OrderNotFound();
        }
        
         $orderDetails= OrderDetails::where('master_order_id' , $checkOrder->id)->whereHas('masterOrder')->with('order')->get();

        $orders = collect($orderDetails)->pluck('order');
        
        $checkDistance = $this->calcCostAndDistance($orders  , $user);
        
        if($checkDistance == false){
            
             return $this->walletChagre();
        }

        return $next($request);
    }
    
    
     function calcCostAndDistance($orders  , $user  ){
         
         // Check The Order Where in Package
        $price_package = PricePackage::all();

        $countOrders = $orders;
        if (count($countOrders) == 1){
            $price = $price_package->where('stage',1)->values();

        }elseif (count($countOrders) == 2 || count($countOrders) == 3 ){
            $price = $price_package->where('stage',2)->values();
        }else{
            $price = $price_package->where('stage',3)->values();
        }
        

        $services_discount = 0;

        foreach ( $orders as $order){

            $kiloMeter = $this->distance($user->latitute, $user->longitute,$order->latitute,$order->longitute,'k');

            // Get The Price Of KiloMeter And Get Max Value if kiloMeter Over
                $valueOfKilo = $price->where('kilometer','>=',$kiloMeter)->first();
                
                $priceKilometer = $valueOfKilo ? $valueOfKilo->price : $price->last()->price;

            $services_discount += $priceKilometer;
        }

        // percent_app
        $percent_app = Setting::where('key','percent_app')->first()->body;
        
        // min_wallet
        $minWallet = Setting::where('key','min_wallet')->first()->body;
        
        $validOrderMax      =   Setting::where('key','max_order')->first()->body;
        
         $walletDriver = User::myTotalWallet($user->id);
         
         
         $valueOrder = ($services_discount * $percent_app ) / 100;
         
         if($valueOrder > $walletDriver && $user['driver_orders']->count() >= $validOrderMax){
             
             return false;
             
         }else{
             
             return true;
         }


    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
    
    

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function notDelivery(){
        return response()->json([   'status' => 401,  'error' => (array) 'يجب التاكد انه  سائق'   ],200);
    }


    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }
    
    
    
    private  function walletChagre(){
        return response()->json([   'status' => 400,  'error' => (array) 'يجب شحن المحفظة لاستكمال الطلب من فضلك' ],200);
    }



}
