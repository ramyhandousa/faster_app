<?php

namespace App\Http\Middleware;

use App\Models\MasterOrder;
use App\User;
use Closure;

class refuseOrderByDelivery
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        // check if not delivery
        if ($user['defined_user'] != 'delivery'):
            return $this->notDelivery();
        endif;

        // check if not Location
        if (!$user['latitute'] || !$user['longitute'] || $user['latitute'] == 'undefined'|| $user['longitute'] == 'undefined'):
            return $this->notHaveLocation();
        endif;

        // check if order not Found
        $masterOrder = MasterOrder::whereId($request->orderId)->first();

        if (!$masterOrder){  return $this->OrderNotFound(); }


        return $next($request);
    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function notDelivery(){
        return response()->json([   'status' => 401,  'error' => (array) 'يجب التاكد انه   مندرب'   ],200);
    }


    private  function notHaveLocation(){
        return response()->json([   'status' => 400,  'error' => (array) 'يجب التاكد انه مندرب لديه مكان'   ],200);
    }

    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }
}
