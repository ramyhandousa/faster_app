<?php

namespace App\Http\Middleware;

use App\Models\Order;
use App\User;
use Closure;

class validProcessOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();
        if ($user['action_provider'] == 'manual'):

            $orders = Order::whereIn('id', $request->orders)->get();

            $flage = $this->checkQuntatiy($orders);

            if ($flage['status'] == false){

                $order = Order::whereId($flage['error'][0]['order_id'])->first();

                return response()->json(['status' => 400 , 'message' =>  ' تم تجهيزه من قبل' . ' الطلب رقم  ' .$order->id ]);
            }

        endif;

        return $next($request);
    }


    private function checkQuntatiy($orders){

        $flage[] = true;
        $error = [];

        foreach ($orders as $item){

            $order = Order::whereId($item->id)->first();


            if ( $order['status_provider'] == 'processing' ){

                $flage[] = false;
                $error[] = [
                    'order_id' => $order->id
                ];
            }
        }

        if (in_array(false , $flage)){
            $data = [ 'status' => false , 'error' => $error];
            return $data;
        }

        $data = [  'status' => true ];
        return $data;
    }

}
