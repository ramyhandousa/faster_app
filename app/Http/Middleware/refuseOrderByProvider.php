<?php

namespace App\Http\Middleware;

use App\Models\MasterOrder;
use App\User;
use Closure;

class refuseOrderByProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        if ($user['defined_user'] != 'provider'):  // check if not provider
            return $this->notProvider();
        endif;

        if (!$request->orders ||count($request->orders) < 0 ): // To make sure you have Orders
            return $this->checkForOrdersQuntatiy();
        endif;

        return $next($request);
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function notProvider(){
        return response()->json([   'status' => 401,  'error' => (array) 'يجب التاكد انه مزود خدمة'   ],200);
    }


    private  function checkForOrdersQuntatiy(){
        return response()->json([   'status' => 400,  'error' => (array) 'يجب إختيار الطلبات المطلوب رفضها'   ],200);
    }

}
