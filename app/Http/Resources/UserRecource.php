<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class UserRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (request()->headers->get('apiToken')){

            $followers = User::where('api_token',request()->headers->get('apiToken'))->first()
                                ->followers->where('id',$this->id)->first() ? true :false;

        }

        return [
            'id' => $this->id,
            'user' => $this->defined_user,
            $this->mergeWhen($this->defined_user == 'provider',
            [
                'action_provider' => $this->action_provider,
                'setting_provider' => $this->setting_provider,
                'link_to_users' => $request->root() .'/' . config('app.locale') .'?provider='. $this->uuid ,
            ]),
            $this->mergeWhen($this->defined_user == 'provider' && $this->setting_provider == 'private',
            [
                'code_for_search' => $this->uuid 
            ]),
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->when($this->email , $this->email) ,
            'image' => $this->when($this->image , $this->image) ,
            'Identity_photo' => $this->when($this->Identity_photo , $this->Identity_photo) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , $this->latitute) ,
            'longitute' => $this->when($this->longitute , $this->longitute) ,
            'is_active' => $this->is_active,
            'is_accepted' => $this->is_accepted,
            'is_suspend' => $this->is_suspend,
            'api_token' => $this->api_token,
            'message' => $this->when($this->message, $this->message) ,
            'city' =>  $this->when($this->city_id , new CityRecource($this->city)),
            $this->mergeWhen(request()->headers->get('apiToken'),
                [
                    'follow' => request()->headers->get('apiToken') ?
                        $followers ? $followers : false
                        : false
                ]),

        ];
    }
}
