<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class driverLoacation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'driver_id' => $this->when($this->driver_id , $this->driver_id) ,
            'latitute' => $this->when($this->latitute , $this->latitute) ,
            'longitute' => $this->when($this->longitute , $this->longitute) ,
            'address' => $this->when($this->address , $this->address) ,
            'date'     => $this->when($this->date , $this->date),
            // 'user_filter' => $this->when($this->driver_id , new UserFilterRecource($this->user_filter)),
        ];
    }
}
