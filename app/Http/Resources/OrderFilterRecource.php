<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderFilterRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'defined_user' => $this->defined_user,
            'phone' => $this->when($this->phone , $this->phone) ,
            'email' => $this->when($this->email , $this->email) ,
            'name' => $this->name,
             'city' =>  $this->when($this->city_id , new CityRecource($this->city)),
            'user_address' => $this->when($this->user_address , $this->user_address) ,
            'address' => $this->when($this->address , $this->address) ,
            'latitute' => $this->when($this->latitute , $this->latitute) ,
            'longitute' => $this->when($this->longitute , $this->longitute) ,
            'description' => $this->when($this->description , $this->description) ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'distance' => $this->when($this->distance , $this->distance) ,
            'cost' => $this->when($this->cost , $this->cost) ,
        ];
    }
}
