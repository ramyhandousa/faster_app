<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'provider_id' => $this->provider_id,
             $this->mergeWhen(
                ($request->route()->uri ==  'api/orders/listPending') ,
                    [
                        'status_provider' => $this->status_provider,
                        'status' => $this->status_provider === 'accepted' ? true: false ,
                    ]),
                
            'cost' => $this->when($this->cost , number_format($this->cost,1)) ,
            'distance' => $this->when($this->distance , number_format($this->distance,0)) ,
            'services_discount' => $this->when($this->services_discount , $this->services_discount) ,
            'percent_app' => $this->when($this->percent_app , $this->percent_app) ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            'provider' =>  $this->when($this->provider_id , new UserFilterRecource($this->provider)),
            'driver' =>   $this->when($this->driver, new UserFilterRecource($this->driver)),
            
             $this->mergeWhen(
                ($request->route()->uri ==  'api/orders/listFinish') ,
                [
                   
                    'driver_location' =>   $this->when($this->driver, new driverLoacation($this->driver_relation)),
                ]),
        ];
    }
}
