<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class notificationRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->when($this->user_id , $this->user_id) ,
            'sender_id' => $this->when($this->sender_id , $this->sender_id) ,
            'order_id' => $this->when($this->order_id , $this->order_id) ,
            'title' => $this->when($this->title , $this->title) ,
            'body' => $this->when($this->body , $this->body) ,
            'cost' => $this->when($this->value != 0 , number_format($this->value,1)) ,
            'type' => $this->when($this->type , $this->type) ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
            
            // 'user' =>  $this->when($this->user_id , new UserFilterRecource($this->user)),
            // 'sender' =>   $this->when($this->sender_id, new UserFilterRecource($this->sender)),
            

        ];
    }
}
