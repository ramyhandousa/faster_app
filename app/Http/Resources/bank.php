<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class bank extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'account_number' => $this->when($this->account_number , $this->account_number) ,
            'iban_number' => $this->when($this->iban_number , $this->iban_number) ,
            'image' => $this->when($this->image , $this->image) ,
        ];
//        return parent::toArray($request);
    }
}
