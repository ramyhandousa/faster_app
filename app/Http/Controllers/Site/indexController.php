<?php

namespace App\Http\Controllers\Site;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Brand;
use App\Models\City;
use App\Models\DealOfDay;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductPurchase;
use App\Models\PurchaseType;
use App\uploadImages;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class indexController extends Controller
{
     public $notify;
        public $push;
    public function __construct(InsertNotification $notification,PushNotification $push )
    {
         
        $this->push = $push;
        $this->notify = $notification; 

    }
    
    public function index(){

        $cities = City::where('is_suspend',0)->get();


        return view('Site.index', compact('cities'));
    }


    public function register(Request $request){
 
        if (!$request->idUser){

            session()->flash('errors', 'تاكد من الرابط من فضلك.');
            return redirect()->back()->withInput(Input::all());

        }
        
         
        $post_data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'city' => $request->city,
            'address' => $request->address, 
            
        ];

        //set Rules .....
        $valRules = [
            'name' => 'required',
            'phone' => 'required',
            // 'email' => 'required',
            'city' => 'required',
            'address' => 'required', 
        ];

        //Declare Validation message
        $valMessages = [
            'name.required' => 'الإسم مطلوب',
            'phone.required' => 'رقم الهاتف مطلوب',
            // 'email.required' => 'البريد الإلكتروني مطلوب',
            'city.required' => '  المدينة مطلوب ',
            'address.required' => '  الموقع مطلوب ',   


        ];

        //validate inputs ......
        $valResult = Validator::make($post_data, $valRules, $valMessages);

        if ($valResult->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($valResult)
                    ->withInput();
        }

        $user = User::where('uuid',$request->idUser)->with('devices')->first();
        $deviceUser =   $user['devices']->pluck('device');

        $order = new Order();
        $order->defined_user = 'user'; 
        $order->user_id = $user->id;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone =$request->phone;
        $order->city_id = $request->city;
        $order->user_address = $request->userAddress;
        $order->address = $request->address;
        $order->latitute = $request->latitute;
        $order->longitute = $request->longitute;
        $order->description = $request->description;
        $order->save();
        
        if(count($deviceUser) > 0){
                   $this->push->sendPushNotification($deviceUser, null, 'الطلبات',
                    '  يوجد لديك طلب جديد رقم '. $order->id . ' من المستخدم' . $request->name,
                    ['type'=> 10,'orderId'=>$order->id]
                );
               }

        $this->notify->NotificationDbType(10,$user->id,null,$request,$order->id);


        return redirect()->route('successRegister');

    }

    public function success(){

        return view('Site.success');
    }


}
