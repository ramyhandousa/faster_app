<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
use Validator;
use UploadImage;
class BrandController extends Controller
{
    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/categories/';
    }

    public function index()
    {
        $brands = Brand::latest()->get();

        $pageName = 'إدارة الماركات';
        return view('admin.brands.index',compact('brands','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageName = 'اسم الماركة ';
        return view('admin.brands.create',compact('pageName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand  = new Brand();
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;

        if ($request->hasFile('image')):
            $brand->image = $request->root() . '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;
        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الماركات']),
            "url" => route('brands.index'),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail($id);
        $pageName = 'اسم الماركة ';

        return view('admin.brands.edit',compact('brand','pageName'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);
        $brand->{'name:ar'} = $request->name_ar;
        $brand->{'name:en'} = $request->name_en;

        if ($request->hasFile('image')):
            $brand->image = $request->root() . '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;
        $brand->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الماركات']),
            "url" => route('brands.index'),

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request){
        if (!Gate::allows('settings_manage')) {
            return abort(401);
        }

        $model = Brand::findOrFail($request->id);

        if (count($model->users) > 0) {

		 return response()->json([
		         'status' => false,
		         'message' => "عفواً, لا يمكنك حذف الماركة نظراً لوجود مستخدمين مشتركين فيها"
		 ]);

	    }

        if (count($model->products) > 0) {

             return response()->json([
                     'status' => false,
                     'message' => "عفواً, لا يمكنك حذف الماركة نظراً لوجود منتجات بها"
             ]);
	    }

        if ($model->delete()) {

            $model->deleteTranslations();

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);

        }

    }



    public function suspend(Request $request)
    {
        $model = Brand::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر الماركة بنجاح";
        } else {
            $message = "لقد تم فك الحظر على الماركة بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


}
