<?php

namespace App\Http\Controllers\Admin;

use App\Commercetype;
use App\Category;
use App\Comment;
use App\Company;
use App\Models\PricePackage;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PostsRequests;
use App\Http\Controllers\Controller;
use Auth;
use Config;
use Image;
use Session;
use UploadImage;
use willvincent\Rateable\Rating;

class SettingsController extends Controller
{

    /**
     * @var string
     * @ public variable to save path.
     */
    public $public_path;

    function __construct()
    {
        $this->public_path = 'files/settings/';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::all();
        return view('admin.settings.setting')->withSettings($settings);
    }


    public function order()
    {
        $settings = PricePackage::all();
        return view('admin.settings.order')->withSettings($settings);
    }

    

    public function aboutus()
    {
        $settings = Setting::all();
        return view('admin.settings.aboutus')->withSettings($settings);
    }

    public function taxs()
    {
        $settings = Setting::all();
        return view('admin.settings.taxs')->withSettings($settings);
    }

    public function terms()
    {
        $settings = Setting::all();
        return view('admin.settings.terms')->withSettings($settings);
    }

    public function suspendElement()
    {
        $settings = Setting::all();
        return view('admin.settings.setting')->withSettings($settings);
    }


    public function support()
    {
        $settings = Setting::all();
        return view('admin.settings.supports')->withSettings($settings);
    }
    
    public function appGeneralSettings()
    {
        return view('admin.settings.general-setting');
    }

    public function contactus()
    {
        $settings = Setting::all();
        return view('admin.settings.contactus')->withSettings($settings);
    }
    
    public function create()
    {

        return view('admin.settings.index');
    }
    
    public function store(Request $request)
    {


        foreach ($request->all() as $key => $value) {
            if ($key != '_token' && $key != 'about_app_image_old'):
                Setting::updateOrCreate(['key' => $key], ['body' => strip_tags($value)]);
            endif;
        }

        if ($request->hasFile('about_app_image')):
            Setting::updateOrCreate(['key' => 'about_app_image'], ['body' => $request->root() . '/public/' . $this->public_path . UploadImage::uploadMainImage($request, 'about_app_image', $this->public_path)]);
//            parse_url('http://www.loadedcamp.net/music/63637-adele-hello', PHP_URL_PATH);
            if ($request->about_app_image_old) {
                if (\File::exists(public_path($request->about_app_image_old))):
                    \File::delete(public_path($request->about_app_image_old));
                endif;
            }
        endif;

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'message' => "لقد تم حفظ بيانات الإعدادات بنجاح",

            ]);
        } else {
            session()->flash('success', 'لقد حفظ الإعدادات بنجاح');
//            Session::flash('message', trans('messages.success'));
            return redirect()->back();
        }


    }


    public function packageStore(Request $request){


        foreach ($request->all() as $key => $value) {
            if ($key != '_token' ):
                $package =   PricePackage::whereId($key)->first();
                $package->update(['price' => $value['price']]);
                $package->update(['kilometer' => $value['kilometer']]);
            endif;
        }

        return response()->json([
            'status' => true,
            'data' => $request->all(),
            'message' => "لقد تم حفظ بيانات الإعدادات بنجاح",

        ]);

    }
    

}
