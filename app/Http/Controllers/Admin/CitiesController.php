<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Countery;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CitiesController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = City::latest();

//        if ($request->type == 'countery'){
//            $cities = $query->where('parent_id',0)->get();
//
//
//            $pageName = 'الدول';
//
//        }else{
//            $cities = $query->where('parent_id','!=',0)->get();
            $cities = $query->get();
            $pageName = 'المدن';
//        }


        return view('admin.cities.index',compact('cities','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//        if ($request->type == 'countery'){
//
//            $pageName = 'الدولة';
//
//        }else{
            $cities = City::latest()->get();

            $pageName = 'المدينة';
//        }
        return view('admin.cities.create',compact('pageName','cities'));
    }

    public function store(Request $request)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $city = new City;
        // if (!request('type')){
        //     $city->parent_id = $request->parentId;
        // }
        $city->{'name:ar'} = $request->name_ar;
        $city->{'name:en'} = $request->name_en;
        $city->save();

//        if ($request->type == 'countery'){
//            $url =  route('cities.index').'?type=countery';
//            $name = 'الدولة';
//        }else{
            $url =  route('cities.index');
            $name = 'المدينة';
//        }
        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => $name]),
            "url" => $url,

        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $city = City::findOrFail($id);


//        if ($request->type == 'countery'){
//
//            $pageName = 'الدول';
//
//        }else{
            $cities = City::get();

            $pageName = 'المدن';
//        }


        return view('admin.cities.edit',compact('city','pageName','cities'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $city = City::findOrFail($id);
        return $city;

//        if (!request('type')){
//            $city->parent_id = $request->parentId;
//        }
        $city->{'name:ar'} = $request->name_ar;
        $city->{'name:en'} = $request->name_en;
        $city->save();

//        if ($request->type == 'countery'){
//            $url =  route('cities.index').'?type=countery';
//            $name = 'الدولة';
//        }else{
            $url =  route('cities.index');
            $name = 'المدينة';
//        }
        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => $name]),
            "url" => $url,

        ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove User from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function groupDelete(Request $request)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $ids = $request->ids;

        $arrsCannotDelete = [];
        foreach ($ids as $id) {
            $model = City::findOrFail($id);

            if ($model->companies->count() > 0) {
                $arrsCannotDelete[] = $model->name;
            } else {
                $model->delete();
            }
        }

        return response()->json([
            'status' => true,
            'data' => [
                'id' => $request->id
            ],
            'message' => $arrsCannotDelete
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $model = City::findOrFail($request->id);

//        if ($model->users->count() > 0) {
//            return response()->json([
//                'status' => false,
//                'message' => "عفواً, لا يمكنك حذف المدينة لوجود شركات بها"
//            ]);
//        }

        if ($model->delete()) {
            $model->deleteTranslations();
            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }


    public function suspend(Request $request)
    {
        $model = City::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم الحظر على مستوي النظام بنجاح";

        } else {
            $message = "لقد تم فك الحظر  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


}
