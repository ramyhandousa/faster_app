<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{

    public function __construct(  )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


    }

    public function getCity( ){

        $data = City::where('is_suspend',0)->select('id')->get();

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }
}
