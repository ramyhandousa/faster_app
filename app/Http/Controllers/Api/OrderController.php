<?php

namespace App\Http\Controllers\Api;

use App\Events\AcceptedOrder;
use App\Events\AccepteOrRefuseDriverOrder;
use App\Events\AccepteOrRefuseProviderOrder;
use App\Events\NotifyUsers;
use App\Http\Requests\api\makeOrder;
use App\Http\Resources\OrderFilterRecource;
use App\Http\Resources\OrderRecource;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Listeners\SendNotificationToUsers;
use App\Mail\AccptedOrder;
use App\Models\City;
use App\Models\DriverNotify;
use App\Models\DriverOrder;
use App\Models\MasterOrder;
use App\Models\PricePackage;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Device;
use App\Models\OrderDetails;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public $headerApiToken;

     public $push;
    public $notify;
    public function __construct(InsertNotification $notification,PushNotification $push )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->push = $push;            
        $this->notify = $notification;
        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

    }

   public function storeOrder(makeOrder $request){

       $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();


       $city  = City::where('is_suspend', 0)->whereId($request->cityId)->first();

       if (!$city){    return $this->CityNotFound(); }

       $order = new Order();
       $order->user_id = $user->id;
       $order->name = $request->name;
       $order->email = $request->email;
       $order->phone =$request->phone;
       $order->city_id = $request->cityId;
       $order->user_address = $request->userAddress;
       $order->address = $request->address;
       $order->latitute = $request->latitute;
       $order->longitute = $request->longitute;
       $order->description = $request->description;
       $order->save();

       $this->createOrderByProvider($user, $order , $request);

       return response()->json( [
           'status' => 200 ,
           'message' => 'تم إنشاء طلبك بنجاح'
       ] , 200 );

   }

    function createOrderByProvider($user , $order , $request){

       $masterOrder = new MasterOrder();
       $masterOrder->provider_id = $user->id;
        $masterOrder->city_id = $request->cityId;
       $masterOrder->save();

       $orderDetails = new OrderDetails();
       $orderDetails->master_order_id = $masterOrder->id;
       $orderDetails->order_id = $order->id;
       $orderDetails->save();

      $this->sendNotification($user ,$masterOrder , $order , $request);
       
       $masterOrder->update(['status_provider' => 'accepted']);
       $order->update(['status_provider' => 'accepted']);

   }

    function sendNotification($user ,$order ,$orders , $request ){

        $drivers  = User::where('defined_user','delivery')->where('city_id',$order->city_id )
            ->where('charge_wallet','false')->whereDoesntHave('roles')->whereDoesntHave('abilities')
            ->whereIsActive(1)->whereIsSuspend(0)->get();

        // Check The Order Where in Package
        $price_package = PricePackage::all();

        $countOrders = $orders;
        if (count($countOrders) == 1){

            $price = $price_package->where('stage',1)->values();

        }elseif (count($countOrders) == 2 || count($countOrders) == 3 ){

            $price = $price_package->where('stage',2)->values();
        }else{

            $price = $price_package->where('stage',3)->values();
        }

        foreach ($drivers as $driver){

            if($driver->latitute &&  $driver->longitute){
                $this->calcCostAndDistanceForDriver($user , $driver,$orders,$price ,$order , $request);
            }

        }
    }

    function calcCostAndDistanceForDriver($provider , $driver , $order , $price , $masterOrder , $request  ){

        $percent_app = Setting::where('key','percent_app')->first();

        // kilo meter of order
        $kiloMeter = $this->distance(floatval($provider->latitute), floatval($provider->longitute),floatval($order->latitute),floatval($order->longitute),'k');

        // Get The Price Of KiloMeter And Get Max Value if kiloMeter Over
        $valueOfKilo = $price->where('kilometer','>=',$kiloMeter)->first();

            $priceKilometer = $valueOfKilo ? $valueOfKilo->price : $price->last()->price;

        // value Cost After percent_app of one order
        $valueCostAfter = number_format($priceKilometer * $percent_app->body /100, 1);

        $this->createDriverDistance($driver,$order,$kiloMeter,$valueCostAfter);

        $this->notify->NotificationDbType(10,$driver->id,$provider->id,$request,$masterOrder->id , number_format( $valueCostAfter, 1) , $kiloMeter );

        $devices = Device::where('user_id',$driver->id)->pluck('device');

        if(count($devices) > 0){
            $this->push->sendPushNotification($devices, null, 'الطلبات',
                ' لديك طلب جديد من مزود الخدمة بقيمة  ' .$valueCostAfter ,
                ['type'=> 10,'orderId'=> $masterOrder->id]
            );
        }
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    function createDriverDistance($driver  , $order , $kilo , $cost){

        $newDistance = new DriverNotify();
        $newDistance->driver_id         = $driver->id;
        $newDistance->order_id          = $order->id;
        $newDistance->kilometer	        = $kilo;
        $newDistance->shipping_cost	    = $cost;
        $newDistance->save();

    }



    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }

    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }


    private  function OrderProviderStatus(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب  تم رفضه او قبوله سابقا من مزود الخدمة'   ],200);
    }

    private  function OrderCheckStatus(){
        return response()->json([   'status' => 400,  'error' => (array) 'يجب قبول او رفض الطلب من مزود الخدمة اولا '   ],200);
    }

    private  function OrderCheckDriverStatus(){
        return response()->json([   'status' => 400,  'error' => (array) 'تم قبول او رفض الطلب سابقا '   ],200);
    }

}
