<?php
    
    namespace App\Http\Controllers\api;
    
    use App\Http\Controllers\Controller;
    use App\Http\Resources\followingRecource;
    use App\Http\Resources\notificationRecource;
    use App\Http\Resources\UserRecource;
    use App\Models\City;
    use App\Models\Notification;
    use App\Models\VerifyUser;
    use App\Models\Setting;
    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;
    use UploadImage;
    use Validator;
    class UsersController extends Controller
    {
    
	 public $public_path;
	 public $defaultImage;
	 public $headerApiToken;
  
	 public function __construct( )
	 {
	        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	        app()->setLocale($language);
	    
	        $this->public_path = 'files/users/profiles/';
	    
	        // default Image upload in profile user
	        $this->defaultImage = " ";
	    
	        // api token from header
	        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
	 }


	 public function listFollowing(){

         $user = User::whereApiToken($this->headerApiToken)->first();

         $data =  followingRecource::collection($user->followers);

         return response()->json([
             'status' => 200,
             'data' =>  $data
         ], 200);
     }

	 public function listFollowers(){

         $user = User::whereApiToken($this->headerApiToken)->first();

         $data =  followingRecource::collection($user->following);

         return response()->json([
             'status' => 200,
             'data' =>  $data
         ], 200);
     }

	 public function followOrUnFollowUser(Request $request){

         $user = User::whereApiToken($this->headerApiToken)->first();


         if(!$user->followers()->where('follow_id', $request->followId)->first()){

            $user->followers()->attach($request->followId);
             return response()->json([
                 'status' => 200,
                 'message' => 'تم اضافته ضمن المتابعين'
             ], 200);

         }else{

             $user->followers()->detach($request->followId);

             return response()->json([
                 'status' => 201,
                 'message' => 'تم  حذفه من المتابعين'
             ], 200);
         }
     }

     public function listProvider(){

          $user = User::whereApiToken($this->headerApiToken)->where('defined_user', 'delivery')->first();
         
         $users = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
                         ->whereIsActive(1)
                         ->whereIsSuspend(0)
                         ->where('defined_user','provider');

         if ($user ){
             
             $ids =  $user->following->pluck('id');
             
             $users->whereNotIn('id' , $ids);
             
         }


         $data = UserRecource::collection($users->get());

         return response()->json( [
             'status' => 200 ,
             'data' => $data ,
         ] , 200 );

     }
	 
	 public function editProfile(Request $request){
        
         $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();
         
         $valResult = Validator::make( $this->DataStudent( $request, $user ) , $this->validData( $user ) , $this->messages());

         if ($valResult->fails()) {
             return response()->json( [ 'status' => 400 , 'error' => $valResult->errors()->all() ] , 200 );
         }

         if ($request->cityId){

             $city  = City::where('is_suspend', 0)->whereId($request->cityId)->first();

             if (!$city){
                 return $this->CityNotFound();
             }
         }

         $action_code = substr(rand(), 0, 4);

         if ($request->phone){
             $checkPhoneChange = $user->wherePhone($request->phone)->count();

             if ($checkPhoneChange == 0){



                 $foundPhone = VerifyUser::whereUserId($user->id)->first();

                 if ($foundPhone){

                     $foundPhone->update(['action_code' => $action_code ]);

                 }else{

                     $verify = new  VerifyUser();
                     $verify->user_id= $user->id;
                     $verify->phone = $request->phone;
                     $verify->action_code = $action_code;
                     $verify->save();

                 }

                 $user->update(['is_active' => 0]);
             }
         }
         
         $user->update($this->DataStudent( $request, $user ));
         $data = new \App\Http\Resources\UserRecource($user);

         return response()->json( [
             'status' => 200 ,
             'code' => $request->phone&&$checkPhoneChange == 0 ?  (string)$action_code : '',
             'message' => 'تم تعديل البيانات بنجاح',
             'data' => $data ,
         ] , 200 );
         
         }

     public function updateSetting(Request $request){
         $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

         if ($request->actionProvider ){

             $user->update(['action_provider' => $request->actionProvider]);
             $data = new UserRecource($user);

             return response()->json( [
                 'status' => 200 ,
                 'data' => $data ,
             ] , 200 );

         }elseif ($request->settingProvider){

             $user->update(['setting_provider' => $request->settingProvider]);
             $data = new UserRecource($user);

             return response()->json( [
                 'status' => 200 ,
                 'data' => $data ,
             ] , 200 );

         }else{

             return response()->json( [
                 'status' => 400
             ] , 200 );

         }

     }
         
     public function getUser(Request $request){

        $user  = User::whereId($request->id)
                        ->whereDoesntHave('roles')
                        ->whereDoesntHave('abilities')
                        ->whereIsActive(1)
                        ->whereIsSuspend(0)
                        ->first();

        if (!$user){  return $this->UserNotFound();  }

        $data = new UserRecource($user);

         return response()->json( [
             'status' => 200 ,
             'data' => $data ,
         ] , 200 );

     }

     public function wallet(){

         $user = User::whereApiToken($this->headerApiToken)->first();

         $wallet = User::myTotalWallet($user->id);
         
         $setting            =   Setting::where('key','percent_app')->first();
         $percent_app        =   $setting  ?   $setting->body : 0;
         $data = [ 'wallet' => $wallet , 'percent_app' => $percent_app ];

         return response()->json( [
             'status' => 200 ,
             'data' => $data ,
         ] , 200 );
     }


    public function notification(Request $request){
        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( ! $user ) {   return $this->UserNotFound();  }

        $notification = notificationRecource::collection( Notification::whereUserId($user->id)->latest()->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $notification,
        ] , 200 );
    }


    
    private function DataStudent ( $request , $user)
    {
        $postData = [
            'name' => $request->name ?$request->name : $user->name ,
            'email' => $request->email ?$request->email : $user->email ,
            'phone' => $request->phone ?$request->phone : $user->phone ,
            'password' => $request->password ,
            'Identity_photo' => $request->Identity_photo ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'Identity_photo' , $this->public_path ): $user->Identity_photo,

            'image' => $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): $user->image ,
            'city_id' => $request->cityId?$request->cityId : $user->city_id,
             'address' => $request->address ?$request->address : $user->address ,
            'latitute' => $request->latitute ?$request->latitute : $user->latitute ,
            'longitute' => $request->longitute ?$request->longitute : $user->longitute ,
            'action_provider' => $request->actionProvider ? $request->actionProvider : $user->action_provider ,
            'setting_provider' => $request->settingProvider ?$request->settingProvider : $user->setting_provider ,
        ];

        return $postData;
    }
    
    
    private function validData ( $user )
    {
        $valRules = [
            'phone' => 'unique:users,phone,' . $user->id ,
            'email' => 'unique:users,email,' . $user->id ,
        ];

        return $valRules;


    }
    
    private function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }
    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }

    private  function adNotFoundByUser(){
        return response()->json([   'status' => 401,  'error' => (array) 'هذ الإعلان  لا ينتمي للمستخدم'   ],200);
    }

    }
