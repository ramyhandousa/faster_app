<?php
    
    namespace App\Http\Controllers\api;
    
    use App\Http\Controllers\Controller;
    use App\Http\Resources\followingRecource;
    use App\Http\Resources\notificationRecource;
    use App\Http\Resources\UserRecource;
    use App\Models\Notification;
    use App\Models\VerifyUser;
    use App\Models\Setting;
    use App\User;
    use Illuminate\Http\Request;
 
    use UploadImage;
    use Validator;
    class PaymentController extends Controller
    {
    


	 public $headerApiToken;
  
	 public function __construct( )
	 {
	        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	        app()->setLocale($language);
	    
	        // api token from header
	        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
	 }


	 public function getPaymentMethods(){

         $user = User::whereApiToken($this->headerApiToken)->first();

    
    
        $token = "DhF6zJJs6vg1Guh-LYtl6IXiPmUhapMc_Rktwa26OTSVwApLM53u_HHVXW55X9_UC-Qx6EJiLLHDpiqVkX2B06jD1JTSecGJ-Awyhyi6CeQE53YNicwYPx7lhhSI06wrK8FaLawQQXAYtBHry8uKZCeF6dncaYb-7JrK2q_LisGDncPf0fHc-A-kgB0WRKs20kwTxXR6VKeb2KiJD78jvfvtOln3wuVZU0G0hNUbhWmsBEQjAuIISYs5xEGTjHN0fdNfbSBGI9HLhYU9sM6btt26wHnvcqlvRiowSB8Aya7LESK5dY8p4iVtRX5gEkoBYwfnibRwS-z-16JCK5-uSl5Z74RdTJncI0_B5cPUvVXBzQE-qEYKUuxEcFWwhPBv7VJ6RxlTcnMfLUxbVJ2qcVXxcmi7dSxTtXYcdoARIWOtM83HFE4KCwDv4Cw6t0npXmhnIowOkUQAaASHaZjzpDtRLYYLfTgGDczyw56sghplRmxoh0_KeQh3lUgUb1pyxlj5m12UIZWAzfxJ2dUJLSIfnfPHu1uU2HKeWd1TVjO5aTR8WJ1Vw-u9qBxpr-YLTTg73X5BMzTTdqSnDs6F4I3pXSih9yjtJMmZ_QxsgaOdhRO6l8SV6bUb6PQ-a7z2xv8dm1KuGHesDarfsjXGFH5u9yx6h6lHWapymLKFzqISv86TIG1wXOdI1axC_Oy94G7Z109VdBohucfif4Va_KEReDvTyXd1c2Wupkzy0xOaECqv"; #token value to be placed here;

        $basURL = "https://api.myfatoorah.com";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$basURL/v2/InitiatePayment",
            CURLOPT_CUSTOMREQUEST => "POST",
            
          CURLOPT_POSTFIELDS => "{\"InvoiceAmount\":0,\"CurrencyIso\": \"SAR\"}",
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);


        $results =  json_decode($response, true);

        if($results['IsSuccess']){
            
            $methods = collect($results['Data']['PaymentMethods'])->whereIn('PaymentMethodId', [2,6])->values()->all();
            return response()->json([
             'status' => 200,
             'data' =>  $methods
         ], 200);
         
        }else{
            return response()->json([
             'status' => 400,
             'message' =>  "Something went wrong."
         ], 400);
        }
        
         
     }
     
     
     public function excutePayment(Request $request){
         
          $user = User::whereApiToken($this->headerApiToken)->first();

        $token = "DhF6zJJs6vg1Guh-LYtl6IXiPmUhapMc_Rktwa26OTSVwApLM53u_HHVXW55X9_UC-Qx6EJiLLHDpiqVkX2B06jD1JTSecGJ-Awyhyi6CeQE53YNicwYPx7lhhSI06wrK8FaLawQQXAYtBHry8uKZCeF6dncaYb-7JrK2q_LisGDncPf0fHc-A-kgB0WRKs20kwTxXR6VKeb2KiJD78jvfvtOln3wuVZU0G0hNUbhWmsBEQjAuIISYs5xEGTjHN0fdNfbSBGI9HLhYU9sM6btt26wHnvcqlvRiowSB8Aya7LESK5dY8p4iVtRX5gEkoBYwfnibRwS-z-16JCK5-uSl5Z74RdTJncI0_B5cPUvVXBzQE-qEYKUuxEcFWwhPBv7VJ6RxlTcnMfLUxbVJ2qcVXxcmi7dSxTtXYcdoARIWOtM83HFE4KCwDv4Cw6t0npXmhnIowOkUQAaASHaZjzpDtRLYYLfTgGDczyw56sghplRmxoh0_KeQh3lUgUb1pyxlj5m12UIZWAzfxJ2dUJLSIfnfPHu1uU2HKeWd1TVjO5aTR8WJ1Vw-u9qBxpr-YLTTg73X5BMzTTdqSnDs6F4I3pXSih9yjtJMmZ_QxsgaOdhRO6l8SV6bUb6PQ-a7z2xv8dm1KuGHesDarfsjXGFH5u9yx6h6lHWapymLKFzqISv86TIG1wXOdI1axC_Oy94G7Z109VdBohucfif4Va_KEReDvTyXd1c2Wupkzy0xOaECqv"; #token value to be placed here;

        $basURL = "https://api.myfatoorah.com";
        
        $fields = '{
  "PaymentMethodId": '.$request->paymentMethodId.',
  "CustomerName": "'.$user->name.'",
  "DisplayCurrencyIso": "SAR",
  "MobileCountryCode": "+966",
  "CustomerMobile": "'.$user->phone.'",
  "CustomerEmail": "'.$user->email.'",
  "InvoiceValue": '.$request->total.',
  "CallBackUrl": "https://faster-app.ml/api/payments/success",
  "ErrorUrl": "https://faster-app.ml/api/payments/errors",
  "Language": "en",
  "CustomerAddress": {
    "Block": "",
    "Street": "",
    "HouseBuildingNo": "",
    "Address": "'.$user->address.'",
    "AddressInstructions": ""
  },
  
  "SupplierCode": 0,
  "InvoiceItems": [
    {
      "ItemName": "Service",
      "Quantity": 1,
      "UnitPrice": '.$request->total.'
    }
  ]

        }';
        
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$basURL/v2/ExecutePayment",
            CURLOPT_CUSTOMREQUEST => "POST",
            
          CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);


        $results =  json_decode($response, true);
        
        
        // return $results;
        
        
         if($results['IsSuccess']){
        
            return response()->json([
             'status' => 200,
             'message'=> $results['Message'],
             'data' =>  $results['Data']
         ], 200);
         
        }else{
            return response()->json([
             'status' => 400,
             'message' =>  $results['Message'],
             'errors' => $results['ValidationErrors']
         ], 400);
        }
        
        
     }
     
     public function success(Request $request){
          return response()->json([
             'status' => 200,
             'message'=> "Success Payment.",
             'data' => $request->all()
         ], 200);
         
     }
     
     public function errors(Request $request){
         
          return response()->json([
             'status' => 400,
             'message'=> "Error Payment.",
             'data' =>  $request->all()
         ], 400);
     }

	 

    }
