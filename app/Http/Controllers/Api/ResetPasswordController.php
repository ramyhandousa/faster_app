<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\changePhone;
use App\Models\VerifyUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Transformers\Json;
use App\User;

use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\Hash;
use App\Http\Helpers\Sms;

class ResetPasswordController extends Controller
{
       
       public $headerApiToken;
    public function __construct(Request $request)
    {

        $language = $request->headers->get('lang') ? $request->headers->get('lang') : 'ar';
        App::setLocale($language);
       
	 // api token from header
	 $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
    }
       
       public function checkCodeActivation(Request $request) {
        
        $user = User::wherePhone($request->phone)->first();
	    
	    if ($user){
	        
            $userCode = VerifyUser::whereUserIdAndActionCode($user->id,$request->code)->first();
        
            if (!$userCode){  return $this->errorActivationCode(); }

               
            $user->update(['phone' => $userCode->phone , 'is_active' => 1]);

            $userCode->delete();

            $data =   new \App\Http\Resources\UserRecource($user);
		 
             return response()->json( [
                     'status' => 200 ,
                     'data' =>$data,
                     'message' =>   trans('global.your_account_was_activated'),
             ] , 200 );
		 
	    }else{
		 
		 return $this->UserNotFound();
		 
	    }
	    
       }
       
       public function changPassword ( Request $request )
       {
	    
	    $user = User::where( 'api_token' , $this->headerApiToken )->first();
	    
	    if ( ! $user ) {  return $this->UserNotFound();  }
	    
	    if ( Hash::check( $request->oldPassword , $user->password ) ) {

	        if ($request->newPassword){
                $user->update( [ 'password' => $request->newPassword ] );

                return response()->json( [
                    'status' => 200 ,
                    'message' =>  trans('global.password_was_edited_successfully')  ,
                ] , 200 );

            }else{
                return response()->json( [
                    'status' => 200 ,
                    'message' =>   trans('global.password_not_edited')
                ] , 200 );
            }

		 
		 
	    }
	    else {
		 return response()->json( [
		         'status' => 400 ,
		         'error' => (array) trans('global.old_password_is_incorrect')  ,
		 ] , 200 );
	    }
	    
       }
       
       public function changePhone(changePhone $request){
	    
	    $user = User::where( 'api_token' , $this->headerApiToken )->first();
	    
	    if ( ! $user ) {  return $this->UserNotFound();  }
	    
	    $userPhone =   User::where('phone' ,$request->oldPhone)->first();
	    
	    if ($userPhone){
		 
		 $rand =  rand();
		 $action_code = substr($rand, 0, 4);
//	 	Sms::sendMessage('Activation code:' . $action_code, $request->newPhone);
		 $verify = new  VerifyUser();
		 $verify->user_id= $user->id;
		 $verify->phone = $request->newPhone;
		 $verify->action_code = $action_code;
		 $verify->save();

		 $user->update(['phone'=> null,'is_active' => 0 ]);
		 return response()->json( [
		         'status' => 200 ,
		         'message' =>   trans('global.activation_code_sent'),
		 ] , 200 );
		 
	    }else{
		 
		 return $this->UserPhoneNotFound();
		 
	    }
	    
       }
       
       public function resendCode ( Request $request )
       {
	    $user = VerifyUser::where('phone',$request->phone)->first();
	    
	    if ($user){
 
		 $action_code = substr(rand(), 0, 4);
		 
//	 	Sms::sendMessage('Activation code:' . $action_code, $request->phone);
		 
		 $user->update(['action_code' => $action_code ]);
		 
		 return response()->json( [
		         'status' => 200 ,
		         'code' => $action_code,
		         'message' =>   trans('global.activation_code_sent'),
		 ] , 200 );
		 
	    }
	    return $this->UserPhoneNotFound();
	    
       }
    
       

       
       private  function UserNotFound(){
	    return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
       }
       private  function errorActivationCode(){
	    return response()->json([   'status' => 400,  'error' => (array) trans('global.activation_code_not_correct')   ],200);
       }
       private  function UserPhoneNotFound(){
	    return response()->json([   'status' => 401,  'error' => (array) trans('global.phone_number_incorrect')   ],200);
       }

}
