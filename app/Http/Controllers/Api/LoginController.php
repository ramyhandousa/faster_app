<?php

namespace App\Http\Controllers\api;
use App\Models\Device;
use App\Http\Requests\api\login;
use App\Models\Order;
use App\Models\VerifyUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\City;
use Illuminate\Support\Facades\DB;

use Validator;
class LoginController extends Controller
{
       
       public $headerApiToken;
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
       
       }
       
    public function login(login $request){

              
      if ($user =  Auth::attempt([  'phone' => $request->phone, 'password' => $request->password  ])  ) {

          $user =  Auth::user();

          $data = new \App\Http\Resources\UserRecource($user);


          $this->manageDevices($request, $user);

          return response()->json([
              'status' => 200,
              'data' => $data,
              'message' =>     trans('global.logged_in_successfully')
          ]);

      }else{

          return response()->json([
              'status' => 401,
              'error' => (array)   trans('global.username_password_notcorrect') ,
          ],200);

      }
  }
    
    public function logOut(Request $request){
    
        $user = User::where( 'api_token' , $this->headerApiToken )->first();
    
        if ( ! $user ) { return   $this->UserNotFound(); }
        
        $this->deleteDevice($request,$user);
    
        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.logged_out_successfully') ,
        ] , 200 );
    
    }
    
    private function manageDevices($request, $user)
    {
        if ($request->deviceToken) {
            
            $data = Device::where('device', $request->deviceToken)->first();
            if ($data) {
                $data->user_id = $user->id;
                $data->save();
            } else {
                $data = new Device();
                $data->device = $request->deviceToken ;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }
            
        }
    }
    
    private function deleteDevice($request ,$userUpdateApi) {
        $device = Device::where('device', $request->deviceToken)->first();
        if ($device){
            $device->delete();
        }
        $userUpdateApi->update([ 'api_token' =>  str_random(60) ]);

    }
    
    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }
    
    
}
