<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\OrderFilterRecource;
use App\Http\Resources\OrderRecource;
use App\Models\DriverNotify;
use App\Models\MasterOrder;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetails;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListOrderController extends Controller
{
    public $headerApiToken;

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->middleware('apiToken');

        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
    }

    public function listWaiting(){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if($user['defined_user'] == 'delivery'){

            $orderId =  Notification::where('user_id',$user->id)->get();
 
            $ids = $orderId->pluck('order_id');

            $orders = MasterOrder::whereIn('id',$ids)->where('status_driver','pending')->whereDate('created_at', '>=', date('Y-m-d'))->get();
            
            $orders->map(function($q) use($orderId){
                $q->cost = $orderId->where('order_id',$q->id)->first() ? $orderId->where('order_id',$q->id)->first()->value : 0;
                $q->distance = $orderId->where('order_id',$q->id)->first() ? $orderId->where('order_id',$q->id)->first()->distance : 0;
            });

            $data = OrderRecource::collection($orders);
            return response()->json( [
                'status' => 200 ,
                'data' => $data
            ] , 200 );

        }else{

            $orders = Order::where('user_id',$user->id)->where('status_provider','pending')->get();

            $data = OrderFilterRecource::collection($orders);

            return response()->json( [
                'status' => 200 ,
                'data' => $data
            ] , 200 );
        }

    }
    
     public function listPending(){
        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $orders =  MasterOrder::where('provider_id',$user->id)
            ->where('status_provider','accepted')->where('status_driver','pending')
            ->OrWhere('provider_id',$user->id)->where('status_provider','processing')
            ->where('status_driver','pending')->get();


         $orders->map(function ($q){
           $q->status  =  $q->status_provider === 'accepted' ? true: false;
        });
        
        $data  = OrderRecource::collection($orders);
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }
    

    public function listAccepted(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if($user['defined_user'] == 'provider'){

            $orders =  MasterOrder::where('provider_id',$user->id)->where('status_provider','accepted')
                                    ->where('status_driver','accepted')->get();
        }else{

            $orders =  MasterOrder::where('driver_id',$user->id)->where('status_provider','accepted')
                                    ->where('status_driver','accepted')->get();
        }
        
         $data  = OrderRecource::collection($orders);

        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }

    public function listFinish(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        if($user['defined_user'] == 'provider'){

            $orders = MasterOrder::where('provider_id',$user->id)->whereStatusProvider('refuse')
                                    ->OrWhere('provider_id',$user->id)->where('status_provider','accepted')
                                    ->where('status_driver','delivered')->get();
                                     
        }else{

            $orders =  MasterOrder::where('driver_id',$user->id)->where('status_provider','accepted')
                                    ->where('status_driver','delivered')->get();
        }
        
         $data  = OrderRecource::collection($orders);
        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }


    public function showMasterOrder(Request $request){

        $order = MasterOrder::whereId($request->orderId)->first();
            if (!$order){  return $this->OrderNotFound(); }

        $orders= OrderDetails::where('master_order_id' , $order->id)->whereHas('masterOrder')->with('order')->get();

        $orders = collect($orders)->pluck('order');
        $orders->map(function ($q){
            $kiloAndCost = DriverNotify::where('order_id',$q->id)->first();
            $q->distance = $kiloAndCost     ? $kiloAndCost->kilometer        : '';
            $q->cost    =  $kiloAndCost     ? $kiloAndCost->shipping_cost	 : '';
        });
        $data = OrderFilterRecource::collection($orders);

            return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }
    
    

    public function showOrder(Request $request){

        $order = Order::whereId($request->orderId)->first();
            if (!$order){  return $this->OrderNotFound(); }
 
            $data = new  OrderFilterRecource($order);

            return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }
    


    public function show(Request $request){

        $order = Order::whereId($request->orderId)->first();

            if (!$order){  return $this->OrderNotFound(); }

        $data = new OrderRecource($order);

        return response()->json( [
            'status' => 200 ,
            'data' => $data
        ] , 200 );
    }


    private  function OrderNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا الطلب غير موجود'   ],200);
    }
}
