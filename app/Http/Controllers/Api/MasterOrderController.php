<?php

namespace App\Http\Controllers\Api;

use App\Events\AccepteOrRefuseDriverOrder;
use App\Events\AccepteOrRefuseProviderOrder;
use App\Events\CalculateOrder;
use App\Events\finishOrder;
use App\Events\NotifyUsers;
use App\Events\WalletEvent;
use App\Models\DriverNotify;
use App\Models\DriverOrder;
use App\Models\MasterOrder;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\PricePackage;
use App\Models\Setting;
use App\Models\Device;
use App\User;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Http\Controllers\Controller;

class MasterOrderController extends Controller
{
    public $headerApiToken;

    public $push;
    public $notify;
    public function __construct(InsertNotification $notification,PushNotification $push )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        $this->push = $push;
        $this->notify = $notification;
        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

    }
    
    
    public function testDistance(){

    }

    public function providerAcceptedOrder(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();


        // first get orders by request array
        $orders = Order::whereIn('id', $request->orders)->get();

        if ($user['action_provider'] == 'manual'){ // to make order processing

            $this->providerProcessing($user , $orders , $request);

            return response()->json( [
                'status' => 200 ,
                'message' => 'سوف يتم تجهيز طلبك الأن',
            ] , 200 );

        }else{
 
            $this->acceptedOrder($request,$user , $orders);

            return response()->json( [
                'status' => 200 ,
                'message' => trans('global.order_accpted'),
            ] , 200 );
        }

    }

    public function pendingOrder(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $masterOrder = MasterOrder::whereId($request->orderId)->first();

        // first get orders by request array
        $orders = Order::whereIn('id', $request->orders)->get();

        $this->sendNotification($user ,$masterOrder , $orders , $request);        // notify Delivery and insert in notification

        $orders->each->update(['status_provider' => 'accepted', 'message' => '']);     // update orders

        $masterOrder->update(['status_provider' => 'accepted']);                        // update Master Order status provider

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.order_accpted'),
        ] , 200 );

    }

    public function refuseOrderByProvider(Request $request){
        $user = User::where( 'api_token' , $this->headerApiToken )->first();

        $orders = Order::whereIn('id', $request->orders)->get();

        $masterOrder = new MasterOrder();
        $masterOrder->provider_id = $user->id;
        $masterOrder->save();

        foreach ($orders as $order):
            $orderDetails = new OrderDetails();
            $orderDetails->master_order_id = $masterOrder->id;
            $orderDetails->order_id = $order->id;
            $orderDetails->save();
        endforeach;

        $orders->each->update(['status_provider' => 'refuse', 'message' => $request->message]);
        $masterOrder->update(['status_provider' => 'refuse', 'message' => $request->message]);
        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.order_refuse'),
        ] , 200 );
    }

    public function acceptedDeliverOrder(Request $request){
        
        $user = User::where( 'api_token' , $this->headerApiToken )->first();


        $order = MasterOrder::whereId($request->orderId)->first();

        event(new AccepteOrRefuseDriverOrder($user, $order, $request));

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.order_accpted'),
        ] , 200 );

    }

    public function refuseDeliveryOrder(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->with('notifications')->first();

        $data = $user['notifications']->where('order_id', $request->orderId);

        if (count($data) > 0){
            $data->each->delete();
        }

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.order_refuse'),
        ] , 200 );

    }

    public function finishOrderByDelivery(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->with('driver_orders')->first();

        $masterOrder = MasterOrder::whereId($request->orderId)->with('orderDetails.order')->first();

        $orders = $masterOrder['orderDetails']->pluck('order');

        \DB::beginTransaction();
        try {

            event(new CalculateOrder($user,$masterOrder, $orders , $request));
            event(new finishOrder($user, $masterOrder , $request));
            event(new WalletEvent($user,$masterOrder, $request));

            \DB::commit();

            return response()->json( [
                'status' => 200 ,
                'message' => 'تم إنتهاء الطلب بنجاح',
            ] , 200 );

        } catch (\Exception $e) {

            \DB::rollback();

            return response()->json( [
                'status' => 400 ,
                'error' => (array) $e->getMessage(),
            ] , 200 );
        }

    }

    private function acceptedOrder($request , $user, $orders){

        // insert in Master Order and Order details
        $masterOrder = new MasterOrder();
        $masterOrder->provider_id = $user->id;
        $masterOrder->city_id = $request->cityId;
        $masterOrder->save();

        foreach ($orders as $order):
            $orderDetails = new OrderDetails();
            $orderDetails->master_order_id = $masterOrder->id;
            $orderDetails->order_id = $order->id;
            $orderDetails->save();
        endforeach;

        $this->sendNotification($user ,$masterOrder , $orders , $request);        // notify Delivery and insert in notification

         $orders->each->update(['status_provider' => 'accepted', 'message' => '']);     // update orders

         $masterOrder->update(['status_provider' => 'accepted']);                        // update Master Order status provider

    }

    private function providerProcessing($user , $orders , $request){

        $masterOrder = new MasterOrder();
        $masterOrder->provider_id = $user->id;
        $masterOrder->city_id = $request->cityId;
        $masterOrder->save();

        foreach ($orders as $order):
            $orderDetails = new OrderDetails();
            $orderDetails->master_order_id = $masterOrder->id;
            $orderDetails->order_id = $order->id;
            $orderDetails->save();
        endforeach;

        $masterOrder->update(['status_provider' => 'processing']);
        $orders->each->update(['status_provider' => 'processing']);
    }


    function sendNotification($user ,$order ,$orders , $request ){
        
         $drivers  = User::where('defined_user','delivery')->where('city_id',$order->city_id )
                            ->where('charge_wallet','false')->whereDoesntHave('roles')->whereDoesntHave('abilities')
                                ->whereIsActive(1)->whereIsSuspend(0)->get();
                                
            // Check The Order Where in Package
        $price_package = PricePackage::all();

        $countOrders = $orders;
        if (count($countOrders) == 1){
            
            $price = $price_package->where('stage',1)->values();

        }elseif (count($countOrders) == 2 || count($countOrders) == 3 ){
            
            $price = $price_package->where('stage',2)->values();
        }else{
            
            $price = $price_package->where('stage',3)->values();
        }
         
    	foreach ($drivers as $driver){

            if($driver->latitute &&  $driver->longitute){
                $this->calcCostAndDistanceForDriver($user , $driver,$orders,$price ,$order , $request);
            }
            
    	}
    }
    

    function calcCostAndDistance($user , $orders , $price  ){

        $services_discount = 0;

        foreach ($orders as $order){

            $kiloMeter = $this->distance(floatval($user->latitute), floatval($user->longitute),floatval($order->latitute),floatval($order->longitute),'k');

            // Get The Price Of KiloMeter And Get Max Value if kiloMeter Over
                $valueOfKilo = $price->where('kilometer','>=',$kiloMeter)->first();

                $priceKilometer = $valueOfKilo ? $valueOfKilo->price : $price->last()->price;

                $services_discount += $priceKilometer;
        }

        // percent_app
        $percent_app = Setting::where('key','percent_app')->first();

        return   number_format($services_discount * $percent_app->body /100, 1);
    }

    function calcCostAndDistanceForDriver($provider , $driver , $orders , $price , $masterOrder , $request  ){
        $services_discount = 0;
        $allKilos = 0;
        $allCost = 0;
        $percent_app = Setting::where('key','percent_app')->first();

        foreach ($orders as $order){

            // kilo meter of order
            $kiloMeter = $this->distance(floatval($provider->latitute), floatval($provider->longitute),floatval($order->latitute),floatval($order->longitute),'k');
            $allKilos += $kiloMeter;
            // Get The Price Of KiloMeter And Get Max Value if kiloMeter Over
                $valueOfKilo = $price->where('kilometer','>=',$kiloMeter)->first();

                $priceKilometer = $valueOfKilo ? $valueOfKilo->price : $price->last()->price;

                $services_discount += $priceKilometer;

                // value Cost After percent_app of one order
                $valueCostAfter = number_format($services_discount * $percent_app->body /100, 1);

                $allCost += $valueCostAfter;

                $this->createDriverDistance($driver,$order,$kiloMeter,$valueCostAfter);
         }

        $this->notify->NotificationDbType(10,$driver->id,$provider->id,$request,$masterOrder->id , number_format( $allCost, 1) , $allKilos );

        $devices = Device::where('user_id',$driver->id)->pluck('device');

            if(count($devices) > 0){
	            $this->push->sendPushNotification($devices, null, 'الطلبات',
	               ' لديك طلب جديد من مزود الخدمة بقيمة  ' .$allCost ,
	                ['type'=> 10,'orderId'=> $masterOrder->id]
	            );
            }
    }


    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }


    function createDriverDistance($driver  , $order , $kilo , $cost){

        $newDistance = new DriverNotify();
        $newDistance->driver_id         = $driver->id;
        $newDistance->order_id          = $order->id;
        $newDistance->kilometer	        = $kilo;
        $newDistance->shipping_cost	    = $cost;
        $newDistance->save();

     }




}
