<?php

namespace App\Http\Controllers\api;

use App\Http\Helpers\Sms;
use App\Http\Requests\api\resgister;
use App\Models\City;
use App\Models\Countery;
use App\Models\Device;
use App\Models\Profile;
use App\Models\VerifyUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class RegisterController extends Controller
{
       public $public_path;
       public $defaultImage;
       public $headerApiToken;
       
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
     
	    $this->public_path = 'files/users/profiles';
	    
	    // default Image upload in profile user
	    $this->defaultImage = " ";
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
       }
       
    public function register(resgister $request){
     
	    $action_code = substr( rand(), 0, 4);

	   $city  = City::where('is_suspend', 0)->whereId($request->cityId)->first();

	   if (!$city){
	       return $this->CityNotFound();
       }

        $user = new User();
        $user->defined_user = $request->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone =$request->phone;
        $user->city_id = $request->cityId;
        $user->password = $request->password;
        $user->address = $request->address;
        $user->latitute = $request->latitute;
        $user->longitute = $request->longitute;
        $user->is_active = 0;
        $user->Identity_photo = $request->Identity_photo ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'Identity_photo' , $this->public_path ): '';
        $user->image = $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): " ";

        if($request->actionProvider || $request->settingProvider){
            $user->action_provider = $request->actionProvider;
            $user->setting_provider = $request->settingProvider;
        }

        $user->save();
        $this->createVerfiy($request , $user , $action_code);
        $this->manageDevices($request, $user);
        $data = new \App\Http\Resources\UserRecource($user);

//	    Sms::sendMessage('Activation code:' . $action_code, $request->phone);

        return response()->json( [
            'status' => 200 ,
            'code' => $action_code,
            'data' => $data
        ] , 200 );
    }

   private function createVerfiy($request , $user , $action_code){
       $verifyPhone = new VerifyUser();
       $verifyPhone->user_id =  $user->id;
       $verifyPhone->phone =    $request->phone;
       $verifyPhone->action_code = $action_code;
       $verifyPhone->save();
   }


    private function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            $data = Device::where('device', $request->deviceToken)->first();
            if ($data) {
                $data->user_id = $user->id;
                $data->save();
            } else {
                $data = new Device();
                $data->device = $request->deviceToken ;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }

        }
    }

    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }
  
}
