<?php

namespace App\Http\Controllers\Api;

use App\Libraries\InsertNotification; 
use App\Models\PricePackage;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypesSupport; 
use App\User; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use UploadImage;
use App\Libraries\PushNotification;

class SettingController extends Controller
{
       public $public_path;
       public $headerApiToken;
       public $language;
       public  $notify;
        public $push;
       public function __construct(InsertNotification $notification,PushNotification $push)
       {
	    $this->language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($this->language);
     
	    $this->public_path = 'files/transfers';
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    $this->notify = $notification;
           $this->push = $push;

       }
       
       
   public function send(){
       
       $data = ['do6kSpmehdc:APA91bEQhbhQb9dU2gjJ-hMiALoiPwmgQHGaiyackti3Y0QM__NRJs3D3ShRh7vMrAqFsLXkGunsdIjmC6ajmYRCLyHxkMGSvI9GKGde9TaymogHZUYe8tTMfoEPBe5eUJfGYHNoy6he'];
       return  $this->push->sendPushNotification($data , null, 'الطلبات',
            ' تم قبول طلبك رقم ',
            ['type'=> 10,'orderId'=>1]
        );
             
    //          $order = Order::whereId(1)->first();
    //          $text = 'سوف يتم تجهيز طلبك الأن';
    //     $message = 'علي الطلب الخاص بك رقم  ';

    //   return  Mail::to($order->email)->send(new AccptedOrder($order, $text, $message));
              
   }
   
   public function listPriceOrder(){
       $prices = PricePackage::all();

       return response()->json( [
           'status' => 200 ,
           'data' => $prices ,
       ] , 200 );
   }
   

   public function aboutUs(){
             
      $about_us =  Setting::whereKey('about_us_' . $this->language)->first();
      $facebook =  Setting::whereKey('contactus_facebook')->first();
      $twitter =  Setting::whereKey('contactus_twitter')->first();
      $inst =  Setting::whereKey('contactus_instagram')->first();
      $phoneCompany =  Setting::whereKey('contactus_phone')->first();
      $emailCompany =  Setting::whereKey('contactus_email')->first();
      $minWallet =  Setting::whereKey('min_wallet')->first();
      $max_order =  Setting::whereKey('max_order')->first();
      

      $data = [
          'about_us' => $about_us ? $about_us->body : '' ,
          'facebbok' => $facebook ? $facebook->body : '' ,
          'twiiter' => $twitter ? $twitter->body : '' ,
          'inst' => $inst ? $inst->body : '' ,
          'phoneCompany' => $phoneCompany ? $phoneCompany->body : '' ,
          'emailCompany' => $emailCompany ? $emailCompany->body : '' ,
          'min_wallet' => $minWallet ? $minWallet->body : '' ,
          'max_order' => $max_order ? $max_order->body : '' ,
          
          ];
	return response()->json( [
	        'status' => 200 ,
	        'data' => $data ,
	] , 200 );
	
   }
   
   public function terms(){
	     
    $terms =   Setting::whereKey(strip_tags('terms_' . $this->language))->first() ;


	return response()->json( [
	        'status' => 200 ,
	        'data' => $terms ?$terms->body : '',
	] , 200 );

   }

   public function contactUs(Request $request){
	    // check for user
	    $user = User::where( 'api_token' , $this->headerApiToken )->first();
	    if ( ! $user ) {   return $this->UserNotFound();  }
	    
	    if ($request->typeId){
	           
	           $type = TypesSupport::whereId($request->typeId)->first();
	           
	           if ($type){
		        $support = new Support();
		        $support->user_id = $user->id;
		        $support->type_id = (int) $request->typeId;
		        $support->message = $request->message;
		        $support->save();

//		        $this->notify->NotificationDbType(2,null,$user->id,$request->message);

		        return response()->json( [
			      'status' => 200 ,
			      'message' => trans('global.message_was_sent_successfully'),
		        ] , 200 );
		 }
		
	    }
	    
	  
   }
       
       
       public function getTypesSupport(){
       
	    $types = TypesSupport::select('id')->get();
	    
	    return response()->json( [
		  'status' => 200 ,
		  'data' => $types ,
	    ] , 200 );
       }
       
       
       public function listBanks(){

        $data = bank::collection(\App\Models\Bank::where('is_active',1)->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }
       
       
       
       
        public function searchCode(Request $request){
            
            if(!$request->code){
                
                return 'فين الكود ياحنان ';
                
            }
       
    	    $user = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
                             ->whereIsActive(1)
                             ->whereIsSuspend(0)
                             ->where('uuid' , $request->code)
                             ->where('defined_user','provider')
                             ->first();
                             
          if(!$user){
                
               if ( ! $user ) {   return $this->UserNotFound();  }
                
            }
                             
             $data = new \App\Http\Resources\UserRecource($user);
    	    
    	    return response()->json( [
    		  'status' => 200 ,
    		  'data' => $data ,
    	    ] , 200 );
       }
       
       private  function UserNotFound(){
	        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
       }
       
       
}
