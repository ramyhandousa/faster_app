<?php

namespace App\Listeners;

use App\Events\WalletEvent;
use App\Models\Setting;
use App\User;
use App\Wallet;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WalletListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WalletEvent  $event
     * @return void
     */
    public function handle(WalletEvent $event)
    {
        $user               =   $event->user;

        $setting            =   Setting::select('id','key','body')->whereIn('key',['percent_app','max_order','pull_wallet'])->get();

        $validPercent_app   =   $setting->where('key','percent_app')->first();
        $validOrderMax      =   $setting->where('key','max_order')->first();
        $pull_wallet        =   $setting->where('key','pull_wallet')->first();

        $percent_app        =   $validPercent_app  ?   $validPercent_app->body : 0;
        $max_order_valid    =   $validOrderMax     ?   $validOrderMax->body    : 0;
        $max_pull_wallet    =   $pull_wallet     ?   $pull_wallet->body    : 0;

        // Check You Have Max Order By admin  && Have percent_app
        if ($user['driver_orders']->count() >= $max_order_valid && $max_order_valid != 0 && $percent_app != 0):

            // Check Wallet Driver Have Money Or Not
            // Check Wallet Have Money Max Admin pull for wallet
                $this->updateWallet($user, $event , $percent_app, $max_pull_wallet);
        endif;
    }


    function updateWallet($user , $event , $percent_app ,$max_pull_wallet ){
        $walletDriver = User::myTotalWallet($user->id);

        $netPaid = ($event->masterOrder->services_discount * $percent_app ) / 100;

        if ($walletDriver > $max_pull_wallet ):

            $wallet = new Wallet();
            $wallet->user_id            = $user->id;
//            $wallet->my_balance = $walletDriver - $netPaid;
            $wallet->order_id           = $event->masterOrder->id;
            $wallet->percent_app        = $percent_app;
            $wallet->paid_balance       = $netPaid;
            $wallet->remaining_balance	= $walletDriver - $netPaid;
            $wallet->save();

        else:
            $user->update(['charge_wallet' => 'true']);
        endif;
    }
}
