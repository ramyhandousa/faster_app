<?php

namespace App\Listeners;

use App\Events\AccepteOrRefuseProviderOrder;
use App\Events\NotifyUsers;
use App\Mail\AccptedOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class AccepteOrRefuseProviderOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccepteOrRefuseProviderOrder  $event
     * @return void
     */
    public function handle(AccepteOrRefuseProviderOrder $event)
    {


       if ($event->request->type == -1){

           $event->order->update(['status_provider' => 'refuse', 'message' => $event->request->message]);

           $text = 'تم رفض  الطلب الخاص بك';
           $message = 'رقم الطلب هو  ';

        //   Mail::to($event->order->email)->send(new AccptedOrder($event->order, $text, $message));


       }else{



           event(new NotifyUsers($event->user, $event->order, $event->request));

           $event->order->update(['status_provider' => 'accepted', 'message' => '']);

           $text = 'تم الموافقة علي الطلب الخاص بك';
           $message = 'رقم الطلب هو  ';

        //   Mail::to($event->order->email)->send(new AccptedOrder($event->order, $text, $message));

       }

    }
}
