<?php

namespace App\Listeners;

use App\Events\finishOrder;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\DriverOrder;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class finishOrderListener
{

    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  finishOrder  $event
     * @return void
     */
    public function handle(finishOrder $event)
    {
        // Update lat && long Order
            $this->updateLatAndLongOrder($event);

        // Send Notification
            $this->sendNotification($event);

        //update Master Order
            $event->order->update([ 'status_driver' => 'delivered' ,'message' => '']);

        // Update Orders To Be finish
             Order::whereIn('id',$event->order->orderDetails->pluck('order_id') )->update(['status_driver' => 'delivered' ,'message' => '']);

    }

    private function updateLatAndLongOrder($event){

        $driverOrder = DriverOrder::whereOrderId($event->request->orderId)->first();

        $driverOrder->update([
            'latitute' => $event->request->latitute    ,
            'longitute' => $event->request->longitute  ,
            'address' => $event->request->address      ,
            'date' => Carbon::now()
        ]);

    }


    private function sendNotification($event){

        $deviceProvider =  Device::where('user_id' , $event->order->provider_id)->pluck('device');

        if(count($deviceProvider) > 0){
            $this->push->sendPushNotification($deviceProvider, null, 'الطلبات',
                ' تم انتهاء طلبك رقم '. $event->order->id . ' من المندوب ' . $event->user->name,
                ['type'=> 12,'orderId'=> $event->order->id]
            );
        }

        $this->notify->NotificationDbType(12,$event->order->provider_id,$event->user,$event->request,$event->order->id);

    }
}
