<?php

namespace App\Listeners;

use App\Events\CalculateOrder;
use App\Models\PricePackage;
use App\Models\Setting;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use function Sodium\increment;

class CalculateOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CalculateOrder  $event
     * @return void
     */
    public function handle(CalculateOrder $event)
    {

        // Check The Order Where in Package
        $price_package = PricePackage::all();

        $countOrders = $event->orders;
        if (count($countOrders) == 1){
            $price = $price_package->where('stage',1)->values();

        }elseif (count($countOrders) == 2 || count($countOrders) == 3 ){
            $price = $price_package->where('stage',2)->values();
        }else{
            $price = $price_package->where('stage',3)->values();
        }

        // Calc the all Cost Delivery && The Distance KiloMeter
         if(request()->route()->uri ==  'api/orders/finishOrder'){
             
            $this->calcCostAndDistance($event, $price);
             
         }else{
            return $this->calcCostAndDistance($event, $price);
         }
    }

    function calcCostAndDistance($event , $price  ){

        $services_discount = 0;

        foreach ($event->orders as $order){

            $kiloMeter = $this->distance($event->user->latitute, $event->user->longitute,$order->latitute,$order->longitute,'k');

            // Get The Price Of KiloMeter And Get Max Value if kiloMeter Over
                $valueOfKilo = $price->where('kilometer','>=',$kiloMeter)->first();
                    $priceKilometer = $valueOfKilo ? $valueOfKilo->price : $price->last()->price;

        if(request()->route()->uri ==  'api/orders/finishOrder'){
            
            $order->update([
                'kilometer'     =>   $kiloMeter,
                'shipping_cost' =>   $priceKilometer
            ]);
            
        }
            $services_discount += $priceKilometer;
        }
        
        
        // percent_app
        $percent_app = Setting::where('key','percent_app')->first();

        if(request()->route()->uri ==  'api/orders/finishOrder'){

            $event->order->update(['services_discount' => $services_discount , 'percent_app' => $percent_app?$percent_app->body:0 ] );
            
        }else{
            return number_format($services_discount * $percent_app->body /100, 1);
        }
        
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }



}
