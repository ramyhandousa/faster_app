<?php

namespace App\Listeners;

use App\Events\AcceptedOrder;
use App\Libraries\InsertNotification;
use App\Mail\AccptedOrder;
use App\Models\DriverOrder;
use App\Models\Notification;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class AcceptedOrderListener
{


    public $notify;

    public function __construct(InsertNotification $notification)
    {
        $this->notify = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  AcceptedOrder  $event
     * @return void
     */
    public function handle(AcceptedOrder $event)
    {

        // Check Driver in database Found Before Or Not
        // Delete Notification Not Belongs to Driver

        $driverOrder = DriverOrder::firstOrCreate(['order_id' => $event->order->id , 'driver_id' => $event->user->id]);

        if ($driverOrder) {

            $driverOrder->update([
                'latitute' => $event->request->latitute    ?$event->request->latitute  : $driverOrder->latitute ,
                'longitute' => $event->request->longitute  ?$event->request->longitute  : $driverOrder->longitute ,
                'address' => $event->request->address      ?$event->request->address  : $driverOrder->address ,
            ]);

        }else{

            $driver = new DriverOrder();
            $driver->driver_id = $event->user->id;
            $driver->order_id = $event->order->id;
            $driver->latitute = $event->request->latitute;
            $driver->longitute = $event->request->longitute;
            $driver->address = $event->request->address;
            $driver->date = Carbon::now();
            $driver->save();
        }

        Notification::where('order_id',$event->order->id)->where('user_id','!=',$event->user->id)->delete();
        $cost = Notification::where('order_id',$event->order->id)->where('user_id',$event->user->id)->first();

        $this->notify->NotificationDbType(11,$event->order->provider_id,$event->user,$event->request,$event->order->id);

        $event->order->update(['driver_id' => $event->user->id ,'cost' => $cost->value , 'status_driver' => 'accepted' ,'message' => '']);

        Order::whereIn('id',$event->order->orderDetails->pluck('order_id') )->update(['status_driver' => 'accepted' ,'message' => '']);


    }
}
