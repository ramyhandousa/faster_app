<?php

namespace App\Listeners;

use App\Events\NotifyUsers;
use App\Libraries\InsertNotification;
use App\Events\CalculateOrder;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Notifications\CommentsNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationToUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */


    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NotifyUsers $event
     * @return void
     */
    public function handle(NotifyUsers $event)
    { 
        $drivers  = User::where('defined_user','delivery')->where('charge_wallet','false')->whereDoesntHave('roles')->whereDoesntHave('abilities')
                                ->whereIsActive(1)->whereIsSuspend(0)->get();
                                // return $drivers;

        $following = $event->user->following->where('defined_user','delivery')->where('charge_wallet','false')->where('is_active',1);

    
        if ($event->user->setting_provider == 'public'){
            
            $devices = Device::whereIn('user_id',$drivers->pluck('id'))->pluck('device');
 
          return   $this->sendFireBase($devices , $event , $drivers);

        }

        if ($event->user->setting_provider == 'private'){

          $devices = Device::whereIn('user_id',$following->pluck('id'))->pluck('device');

          $this->sendFireBase($devices , $event , $following);
                
        }

    }


    private function sendFireBase($devices , $event ,$drivers = []){
        if(request()->route()->uri ==  'api/orders/deliverAccepted' && count($devices) > 0){

            $this->push->sendPushNotification($devices, null, 'الطلبات',
                ' تم قبول طلبك رقم '. $event->order->id . ' من المندوب ' . $event->user->name,
                ['type'=> 11,'orderId'=>$event->order->id]
            );
            
            
        }else{
             
        	foreach ($drivers as $driver){ 
	            $cost =  event(new CalculateOrder($driver,$event->order, $event->orders , $event->request));
	            
	             if(count($devices) > 0){
		            $this->push->sendPushNotification($devices, null, 'الطلبات',
		               ' لديك طلب جديد من مزود الخدمة بقيمة  ' .$cost[0] ,
		                ['type'=> 10,'orderId'=>$event->order->id]
		            );
	            }
	      
	            $this->notify->NotificationDbType(10,$driver->id,$event->user->id,$event->request,$event->order->id , $cost[0]);
        	}
        	
        } 

    }
    
    
}
