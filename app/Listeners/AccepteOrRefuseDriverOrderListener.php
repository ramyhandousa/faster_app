<?php

namespace App\Listeners;

use App\Events\AcceptedOrder;
use App\Libraries\InsertNotification;
use App\Events\AccepteOrRefuseDriverOrder;
use App\Models\Order;
use App\Models\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccepteOrRefuseDriverOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $notify;

    public function __construct(InsertNotification $notification)
    {
        $this->notify = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  AccepteOrRefuseDriverOrder  $event
     * @return void
     */
    public function handle(AccepteOrRefuseDriverOrder $event)
    {


//       if ($event->request->type == -1){
//
//
//           Notification::where('order_id',$event->order->id)->where('user_id','=',$event->user->id)->delete();
//
//       }else{

           event(new AcceptedOrder($event->user,$event->order,$event->request));

//       }


    }
}
