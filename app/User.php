<?php

namespace App;

use App\Models\Ad;
use App\Models\City;
use App\Models\Conversation;
use App\Models\Countery;
use App\Models\Device;
use App\Models\MasterOrder;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Product;
use App\Traits\UUID;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;
use willvincent\Rateable\Rateable;
use willvincent\Rateable\Rating;

class User extends Authenticatable
{
    use Notifiable ,HasRolesAndAbilities, UUID ,Rateable;


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
        
    }

    protected $fillable = [
        'name',
        'phone',
        'email',
        'city_id',
        'password',
        'image',
        'Identity_photo',
        'latitute',
        'longitute',
        'address',
        'action_provider',
        'setting_provider',
        'uuid',
        'api_token',
        'is_active',
        'is_accepted',
        'is_suspend',
        'charge_wallet',
        'message',
        'login_count',
    ];

    
    protected $hidden = [
        'password','remember_token'
    ];


    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }
            
        } else {
            redirect(route('admin.login'));
        }
    }
    
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }


    public function city()
    {
        return $this->belongsTo(City::class);
    }


   
    public function ratings()
    {
        return $this->morphMany('willvincent\Rateable\Rating', 'rateable');
    }



    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }


    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function driver_orders()
    {
        return $this->hasMany(MasterOrder::class ,'driver_id');
    }

    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follow_id');
    }


    public function following()
    {
        return $this->belongsToMany(User::class, 'followers', 'follow_id', 'user_id');
    }


    public static function myTotalWallet($id){

        $myTotal = User::whereId($id)->whereHas('wallets')->first();

        if ($myTotal){

            $wallets = $myTotal->wallets;

            $deposit = $wallets->where('deposit', "false")->values();
            $incoming = $wallets->where('deposit', "true")->values();

           if ($deposit->count()){

               return $deposit->pluck('remaining_balance')->last();
           }else{

               return $incoming->pluck('my_balance')->last();
           }

        }else{

            return 0;
        }

    }



}
