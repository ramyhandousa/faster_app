@extends('admin.layouts.master')

@section('title', 'المستخدمين')
@section('styles')

    <!-- Custom box css -->
    <link href="{{ request()->root() }}/public/assets/admin/plugins/custombox/dist/custombox.min.css" rel="stylesheet">




    <style>
        .errorValidationReason{

            border: 1px solid red;

        }
    </style>
@endsection
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15 ">

            </div>
            <h4 class="page-title">@lang('maincp.customer_management') </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="zmdi zmdi-more-vert"></i> --}}
                    {{--</a>--}}

                </div>

                <h4 class="header-title m-t-0 m-b-30">
                    {{--@lang('trans.managers_system')--}}
                </h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>@lang('trans.username')</th>
                        <th>@lang('maincp.mobile_number')</th>
                        <th>@lang('trans.status' ) </th>
                        {{--<th>مفعل</th>--}}
                        <th>@lang('trans.created_at')</th>
                        <th>@lang('trans.options')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $row)
                        <tr>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>
                                <div class="StatusActive{{ $row->id }}"  style="display: {{ $row->is_suspend == 0 ? "none" : "block" }}; text-align: center;">
                                    <img  width="23px" src="{{ request()->root() }}/public/assets/admin/images/false.png" alt="">
                                </div>
                                <div class="StatusNotActive{{ $row->id }}" style="display: {{ $row->is_suspend == 0 ? "block" : "none" }};  text-align: center;">
                                    <img width="23px" src="{{ request()->root() }}/public/assets/admin/images/ok.png" alt="">
                                </div>

                            </td>

                            <td>{{ $row->created_at != ''? @$row->created_at->format('Y/m/d'): "--" }}</td>
                            <td>

                                <a href="{{ route('users.show', $row->id) }}"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="@lang('institutioncp.show_details')"
                                   class="btn btn-icon btn-xs waves-effect  btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="0"
                                   data-url="{{ route('user.suspend') }}"  style="@if($row->is_suspend == 0) display: none;  @endif"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  suspendElement suspend{{ $row->id }}"
                                   id="suspendElement" data-message="تاكيد التفعيل"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="فك الحظر ">
                                    <i class="fa fa-unlock"></i>
                                </a>

                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="1"
                                   data-url="{{ route('user.suspend') }}" style="@if($row->is_suspend == 1) display: none;  @endif"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill suspendElement unsuspend{{ $row->id }}"
                                   id="suspendElement"
                                   data-message="حظر"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="{{ __('trans.suspend') }}">
                                    <i class="fa fa-lock"></i>
                                </a>

                                @if($row->is_accepted == 0)
                                        <a   href="javascript:;"
                                             class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  "
                                             data-toggle="modal" data-target="#m_modal_7{{ $row->id }}">
                                            قبول
                                        </a>
                                @endif   
                                
                                 {{--  accepted --}}
                            <div class="modal fade" id="m_modal_7{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">  قبول الطلب </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <form method="POST" action="{{ route('providers.acceptedOrRefuse') }}?type=accepted">

                                                {{ csrf_field() }}
                                                {{--<div class="form-group">--}}
                                                {{--<label for="message-text" class="form-control-label">Message:</label>--}}
                                                <input type="hidden" name="id" value="{{$row->id}}">
                                                {{--<textarea class="form-control" name="message" id="message-text"></textarea>--}}
                                                {{--</div>--}}

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">إلغاء</button>
                                                    <button type="submit" class="btn btn-primary">قبول</button>
                                                </div>

                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end::Modal-->

                                         

                                     
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')

    <!-- Modal-Effect -->
    <script src="{{ request()->root() }}/public/assets/admin/plugins/custombox/dist/custombox.min.js"></script>
    <script src="{{ request()->root() }}/public/assets/admin/plugins/custombox/dist/legacy.min.js"></script>


    <script>


        $(document).ready(function () {
            //$('#datatable').dataTable();
            //$('#datatable-keytable').DataTable( { keys: true } );
            $('#datatable-responsive').DataTable();

        });

        $('body').on('click', '.accepted', function () {
            var id = $(this).attr('data-id');
            swal({
                title: "قبول الطلب؟",
                text: "",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-success waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('user.accepted') }}',
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {
                            if (data) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت القبول  بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-center',
                                    onclick: null,
                                    showMethod: 'slideDown',
                                    hideMethod: "slideUp",
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 1200);
                            }

                        }
                    });
                } else {

                }
            });
        });


    </script>


@endsection



