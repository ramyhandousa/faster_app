@extends('admin.layouts.master')
@section('title', __('maincp.user_data'))
@section('content')


    <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('users.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">@lang('maincp.user_data') </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            @if($user->defined_user == 'provider')
                                                <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="true">بيانات إضافية </a></li>
                                            @endif
                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>@lang('maincp.personal_data')</h4>
                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.full_name') :</label>
                                                            <input class="form-control" value="{{ $user->name }}" readonly><br>
                                                        </div>

                                                        @if($user->phone )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.mobile_number') :</label>
                                                                <input class="form-control" value="{{ $user->phone }}" readonly><br>
                                                            </div>
                                                        @endif

                                                        @if($user->email )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.e_mail')  :</label>
                                                                <input class="form-control" value="{{ $user->email }}" readonly><br>
                                                            </div>
                                                        @endif

                                                        @if($user->city )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>المدينة  :</label>
                                                                <input class="form-control" value="{{ optional($user->city)->name  }}" readonly><br>
                                                            </div>
                                                        @endif

                                                        @if($user->city->parent )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>الدولة  :</label>
                                                                <input class="form-control" value="{{ optional($user->city->parent)->name  }}" readonly><br>
                                                            </div>
                                                        @endif
                                                        @if($user->defined_user == 'provider' )
                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.address') :</label>
                                                            <input class="form-control" value="{{ $user->address }}" readonly><br>
                                                        </div>
                                                        @endif
                                                        {{--<div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">--}}
                                                            {{--<label>@lang('maincp.account_access_count'):</label>--}}
                                                            {{--<input class="form-control" value="{{ $user->login_count > 0 ? $user->login_count : 0 }} @lang('maincp.once')"><br>--}}
                                                        {{--</div>--}}
                                                    </div>



                                                    <div class="col-sm-6">
                                                        <div class="card-box">
                                                            <div class="row">

                                                                <div class="card-box" style="overflow: hidden;">
                                                                    <h4 class="header-title m-t-0 m-b-30">@lang('institutioncp.personal_image')</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage($user->image, request()->root().'/public/assets/admin/images/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage($user->image, request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    

                                                                </div>
                                                                @if($user->Identity_photo)
                                                                <div class="card-box" style="overflow: hidden;">
                                                                <h4 class="header-title m-t-0 m-b-30">صورة الهوية</h4>
                                                                     <div class="form-group">
                                                                        
                                                                         <div class="col-sm-6">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage($user->Identity_photo, request()->root().'/public/assets/admin/images/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage($user->Identity_photo, request()->root().'/public/assets/admin/images/default.png') }}"/>
                                                                            </a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                @endIf
                                                                
                                                                

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane m-t-10 fade " id="tab2">

                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>بيانات إضافية </h4>
                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>تفعيل الطلب :</label>
                                                            <input class="form-control" value="{{ $user->action_provider }}" readonly><br>
                                                        </div>


                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>حالة الشخصية  : عامة / خاصة</label>
                                                            <input class="form-control" value="{{ $user->setting_provider }}" readonly><br>
                                                        </div>


                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>لينك السوشيال ميديا  :</label>
                                                            {{--<input class="form-control" value="{{'https://faster-app.ml/ar?provider='. $user->uuid }}"><br>--}}
                                                            <input class="form-control" value="{{url('/ar?provider=') . $user->uuid }}" readonly><br>
                                                        </div>



                                                    </div>




                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>


    </form>

@endsection

