<!-- Navigation Bar-->
<header id="topnav">

    <div class="topbar-main">
        <div class="container">

            <!-- LOGO -->
            <div class="topbar-left">
                {{--<a href="{{ route('admin.home') }}" class="logo" style="width: 150px;">--}}
                    {{--<img style="width: 100%" src="{{ request()->root() }}/public/assets/admin/images/logo.png"></a>--}}
            </div>
            <!-- End Logo container-->


            <div class="menu-extras">

                <ul class="nav navbar-nav navbar-right pull-right">
                    {{--<li>--}}
                    {{--<forsm role="search" class="navbar-left app-search pull-left hidden-xs">--}}
                    {{--<input type="text" placeholder="بحث ..." class="form-control">--}}
                    {{--<a href=""><i class="fa fa-search"></i></a>--}}
                    {{--</form>--}}
                    {{--</li>--}}

                    <!--<li>-->
                    <!--    <div class="notification-box">-->
                    <!--        <ul class="list-inline m-b-0">-->
                    <!--            <li>-->
                    <!--                {{--<a href="{{ route('notifications.index') }}" class="right-bar-toggle">--}}-->
                    <!--                <a href="#" class="right-bar-toggle">-->
                    <!--                    <i class="zmdi zmdi-notifications-none"></i>-->
                    <!--                </a>-->
                    <!--                <div class="noti-dot">-->
                    <!--                    <span class="dot"></span>-->
                    <!--                    <span class="pulse"></span>-->
                    <!--                </div>-->
                    <!--            </li>-->
                    <!--        </ul>-->
                    <!--    </div>-->
                    <!--</li>-->

                    {{--<li class="dropdown">--}}
                        {{--<a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown"--}}
                           {{--aria-expanded="true">--}}
                            {{--<img src="{{ request()->root() }}/public/assets/admin/images/saudi-arabia.png"--}}
                                 {{--alt="user-img"--}}
                                 {{--class="img-circle user-img">--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu">--}}

                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                            {{--{{ app()->getLocale() }} <i class="fa fa-caret-down"></i>--}}
                            {{--</a>--}}

                            {{--@foreach (config('translatable.locales') as $lang => $language)--}}
                                {{--@if ($lang != app()->getLocale())--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ route('lang.switch', $lang) }}">--}}
                                            {{--{{ $language }}--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}


                        {{--</ul>--}}
                    {{--</li>--}}

                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown"
                           aria-expanded="true">
                            <img src="{{ $helper->getDefaultImage(auth()->user()->image, request()->root().'/public/assets/admin/images/default.png') }}"
                                 alt="user-img" class="img-circle user-img">
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="{{ route('helpAdmin.edit', auth()->user()->id)}}"><i
                                            class="ti-user m-r-5"></i>@lang('maincp.personal_page')</a></li>
{{--                            <!--<li><a href="{{ route('users.edit', auth()->id()) }}"><i class="ti-settings m-r-5"></i>-->--}}
                            <!--        @lang('global.settings')-->
                            <!--    </a>-->
                            <!--</li>-->
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="ti-power-off m-r-5"></i>@lang('maincp.log_out')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>

    <form id="logout-form" action="{{ route('administrator.logout') }}" method="POST"
          style="display: none;">
        {{ csrf_field() }}
    </form>

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu" style="    font-size: 14px;">
                    <li>
                        <a href="{{ route('admin.home') }}"><i class="zmdi zmdi-view-dashboard"></i>
                            <span> @lang('menu.home') </span> </a>
                    </li>

                    @can('companies_management')
                        <li class="has-submenu">
                            <a href="#"><i
                                        class="zmdi zmdi-accounts"></i><span>@lang('maincp.customer_management')   </span>
                            </a>
                            <ul class="submenu ">
                                <li>  <a href="{{ route('users.index') }}?type=providers">  مزودي الخدمات </a>   </li>
                                <li>  <a href="{{ route('users.index') }}?type=drivers">  السائقين  </a>   </li>
                            </ul>
                        </li>

                    @endcan


                    <!--@can('users_manage')-->
                    <!--<li class="has-submenu">-->
                    <!--    <a href="#"><i class="zmdi zmdi-layers"></i><span>  @lang('trans.managers_ctrl_panel') </span>-->
                    <!--    </a>-->
                    <!--    <ul class="submenu ">-->
                    <!--        <li><a href="{{ route('helpAdmin.index') }}"> @lang('trans.managers_system')</a></li>-->
                    <!--        <li>-->
                    <!--            <a href="{{route('roles.index')}}">  @lang('trans.roles_and_permission')     </a>-->
                    <!--        </li>-->

                    <!--    </ul>-->
                    <!--</li>-->
                    <!--@endcan-->



                    @can('content_management')
                    <li class="has-submenu">
                        <a href="javascript:;">
                            <i class="zmdi zmdi-square-o"></i>
                            <span>  @lang('maincp.content_management')  </span>
                        </a>
                        <ul class="submenu ">

                            {{--<li><a href="{{ route('cities.index') }}?type=countery">الدول</a></li>--}}
                            <li><a href="{{ route('cities.index') }}">@lang('trans.cities')</a></li>
{{--                            <li><a href="{{ route('banks.index') }}">@lang('trans.banks_accounts')</a></li>--}}
                            {{--<li><a href="{{ route('categories.index') }}">الأقسام الرئيسة</a></li>--}}
                            {{--<li><a href="{{ route('categories.index') }}?type=subcategories">الأقسام الفرعية</a></li>--}}
                            {{--<li><a href="{{ route('ads.index') }}">  الإعلانات</a></li>--}}


                        </ul>
                    </li>

                    @endcan


                    @can('app_general_settings_management')


                    <li class="has-submenu">
                        <a href="#"><i class="zmdi zmdi-settings"></i><span>@lang('maincp.setting')<i
                                        class="fa fa-arrow-down visible-xs" aria-hidden="true"></i></span> </a>
                        <ul class="submenu">
                            <li><a href="{{ route('settings.order') }}">التحكم في الطلبات</a>    </li>
                            <li><a href="{{ route('settings.contactus') }}">@lang('trans.general_setting_app')</a>    </li>

                            <li><a href="{{ route('settings.aboutus') }}">@lang('trans.about_app')</a>   </li>
                            <li><a href="{{ route('settings.terms') }}">@lang('trans.terms')</a></li>
                            <li><a href="{{ route('types.index') }}">انواع رسائل التواصل</a></li>
                            <li><a href="{{ route('banks.index') }}">@lang('trans.banks_accounts')</a></li>
                            <li><a href="{{ route('supports.index') }}">@lang('maincp.contact_us')</a></li>
                        </ul>
                    </li>
                    @endcan


                  @can('orders_management')

                    <li class="has-submenu">
                        <a href="#"><i class="zmdi zmdi-border-all"></i>
                            <span>
                                {{--@lang('trans.orders_management')   --}}
                                إدارة التحويلات
                            </span>
                        </a>
                        <ul class="submenu ">
                            <li>
                                {{--<a href="{{ route('reports.index') }}">         @lang('trans.bank_transfer_from_client')  </a>--}}
                                <a href="{{ route('reports.index') }}">         @lang('trans.reports_orders')  </a>
                            </li>

                        </ul>
                    </li>

                    @endcan



                    @can('reports_management')
                    <li class="has-submenu">
                        {{--<a href="#"><i class="zmdi zmdi-flag"></i><span>@lang('maincp.reports')</span> </a>--}}
                        <ul class="submenu">

                        </ul>
                    </li>
                    @endcan

                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>

</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container">
