@extends('admin.layouts.master')

@section("title", __("maincp.call_us"))
@section('styles')

    <style>
        .customeStyleSocail{

            margin: 10px auto;

        }
    </style>
@endsection
@section('content')
    <form action="{{ route('administrator.package.store') }}" data-parsley-validate
          novalidate method="post"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-0">
                    <div class="btn-group pull-right m-t-15">
                        <button type="button" class="btn btn-custom  waves-effect waves-light"
                                onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                        class="fa fa-reply"></i></span>
                        </button>
                    </div>

                </div>
                <h4 class="page-title">  التحكم في أسعار الطلبات  </h4>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive m-t-0">

                    <div class="form-group">
                        <section class="pricing py-5">
                            <div class="container">
                                <div class="row">
                                    <!-- Free Tier -->
                                    <div class="col-lg-4">
                                        <div class="card mb-5 mb-lg-0">
                                            <div class="card-body">
                                                <h5 class="card-title text-muted text-uppercase text-center">المرحلة الأولي</h5>
                                                <h6 class="card-price text-center"> <span class="period">تنقسم إالي 3 أجزاء </span></h6>
                                                <hr>
                                                <ul class="fa-ul">
                                                    <div class="col-md-6">
                                                        <span class="input-group-addon" id="basic-addon2">  السعر</span>
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(1)}}" name="1[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <span class="input-group-addon">  المسافة بالكيلو</span>
                                                        <input type="text"  class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(1)}}" name="1[kilometer]" >
                                                        <br>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(2)}}" name="2[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text"  class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(2)}}" name="2[kilometer]" >
                                                        <br>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text"class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(3)}}" name="3[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(3)}}" name="3[kilometer]" >
                                                    </div>
                                                 </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Plus Tier -->
                                    <div class="col-lg-4">
                                        <div class="card mb-5 mb-lg-0">
                                            <div class="card-body">
                                                <h5 class="card-title text-muted text-uppercase text-center">المرحلة الثانية</h5>
                                                <h6 class="card-price text-center"><span class="period">تنقسم إالي جزئين</span></h6>
                                                <hr>
                                                <ul class="fa-ul">
                                                    <div class="col-md-6">
                                                        <span class="input-group-addon" id="basic-addon2">  السعر</span>
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(4)}}" name="4[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <span class="input-group-addon">  المسافة بالكيلو</span>
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(4)}}" name="4[kilometer]" >
                                                        <br>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(5)}}" name="5[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text"  class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(5)}}" name="5[kilometer]" >
                                                    </div>
                                                 </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Pro Tier -->
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title text-muted text-uppercase text-center">المرحلة الثالثة</h5>
                                                <h6 class="card-price text-center"><span class="period">تنقسم الي جزئين</span></h6>
                                                <hr>
                                                <ul class="fa-ul">
                                                    <div class="col-md-6">
                                                        <span class="input-group-addon" id="basic-addon2">  السعر</span>
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(6)}}" name="6[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <span class="input-group-addon">  المسافة بالكيلو</span>
                                                        <input type="text"  class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(6)}}" name="6[kilometer]" >
                                                        <br>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getPrice(7)}}" name="7[price]" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input type="text"  class="form-control percent_app" min="1" max="1000" value="{{$pricePakage->getKilo(7)}}" name="7[kilometer]" >
                                                    </div>
                                               </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <br>
                        <div class="col-xs-12 text-right">

                            <button type="submit" class="btn btn-warning">
                                @lang('maincp.save_data')   <i style="display: none;" id="spinnerDiv"
                                                               class="fa fa-spinner fa-spin"></i>
                            </button>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-ar.js"></script>


    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            console.log(formData);
            $('#spinnerDiv').show();
            var form = $(this);
            console.log(form);
            form.parsley().validate();
            if (form.parsley().isValid()) {
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {


                        if (data.status == true) {
                            //  $('#messageError').html(data.message);
                            $('#spinnerDiv').hide();
                            var shortCutFunction = 'success';
                            var msg = data.message;
                            var title = 'نجاح';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;
                        }

                        setTimeout(function () {
                        window.location.href = '{{ route('settings.order') }}';
                        }, 1500);
                    },
                    error: function (data) {
                    }
                });
            }
        });

    </script>
@endsection







