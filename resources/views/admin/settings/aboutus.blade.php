@extends('admin.layouts.master')
@section('title' ,__('maincp.about_us'))
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">

    {{ csrf_field() }}

    <!-- Page-Title -->

        <div class="row">
            <div class="col-sm-12 ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">@lang('maincp.data_about_the_application') </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('about_app_desc') ? 'has-error' : '' }}">
                            <label for="about_app_desc">@lang('maincp.content')  </label>
                            <textarea id="editor100200" class="form-control msg_body" required
                                      name="about_app_desc_ar">{{ $setting->getBody('about_app_desc_ar') }}</textarea>
                        </div>
                    </div>


                    <!-- <div class="col-xs-6">-->
                    <!--<div class="form-group">-->
                    <!--    <div class="col-sm-12">-->

                    <!--        <input type="hidden" name="about_app_image_old"-->
                    <!--               value="{{ $setting->getBody('about_app_image') }}">-->
                    <!--        <input type="file" name="about_app_image" class="dropify" data-max-file-size="6M"-->
                    <!--               data-default-file="{{  $setting->getBody('about_app_image') }}" data-show-remove="false"-->
                    <!--           data-allowed-file-extensions="pdf png gif jpg jpeg"-->
                    <!--           data-errors-position="outside"/>-->

                    <!--    </div>-->
                    <!--</div>-->
                    <!--</div>-->

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
 <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
<script>


        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor2');

</script>

@endsection




