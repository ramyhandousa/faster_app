@extends('admin.layouts.master')
@section('title' ,__('maincp.about_us'))
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">

    {{ csrf_field() }}

    <!-- Page-Title -->

        <div class="row">
            <div class="col-lg-12 ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">@lang('trans.general_setting_app') </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 ">
                <div class="card-box">

                    {{--<h4 class="header-title m-t-0 m-b-30">@lang('maincp.data_about_the_application')  </h4>--}}

                    <div class="col-xs-12">
                        <div class="form-group">


                            <label for="userName">@lang('trans.app_tax_every_order') *</label>
                            <input type="text" name="app_tax_every_order"
                                   value="{{ $setting->getBody('app_tax_every_order') }}"
                                    class="form-control numbersOnly onlyNumber" data-limit="10"
                                       data-message=""
                                   required
                                   data-parsley-required-message="@lang('maincp.required') "
                                   placeholder="@lang('trans.app_tax_every_order')..."/>
                            <p class="help-block"></p>

                        </div>

                    </div>


                    <div class="col-xs-12">
                        <div class="form-group">


                            <label for="userName">@lang('trans.app_tax_every_month') *</label>
                            <input type="text" name="app_tax_every_month"
                                   value="{{ $setting->getBody('app_tax_every_month') }}"
                                    class="form-control numbersOnly onlyNumber" data-limit="10"
                                       data-message=""
                                   required  data-parsley-required-message="@lang('maincp.required') "
                                   placeholder="@lang('trans.app_tax_every_month')..."/>
                            <p class="help-block"></p>

                        </div>

                    </div>


                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">@lang('trans.app_tax_every_finish_order') *</label>
                            <input type="text" name="app_tax_every_finish_order"
                                   value="{{ $setting->getBody('app_tax_every_finish_order') }}"
                                    class="form-control numbersOnly onlyNumber" data-limit="10"
                                       data-message=""
                                   required  data-parsley-required-message="@lang('maincp.required') "
                                   placeholder="@lang('trans.app_tax_every_finish_order')..."/>
                            <p class="help-block"></p>

                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">@lang('trans.app_contact_phone') *</label>
                            <input type="text" name="app_contact_phone"
                                   value="{{ $setting->getBody('app_contact_phone') }}"
                                   class="form-control phone numbersOnly"  data-limit="10"
                                   required data-message="{{ __('trans.maxNumeber', ['number' => 10]) }}"
                                   data-parsley-required-message="@lang('maincp.required') "
                                   placeholder="@lang('trans.app_contact_phone')..."/>
                            <p class="help-block"></p>

                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">@lang('trans.app_contact_email') *</label>
                            <input type="text" name="app_contact_email"
                                   value="{{ $setting->getBody('app_contact_email') }}"
                                   class="form-control email" data-parsley-trigger="keyup"
                                   required  data-parsley-required-message="@lang('maincp.required') "
                                   placeholder="@lang('trans.app_contact_email')..."/>
                            <p class="help-block"></p>
                        </div>
                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script>
        $('form').on('submit', function (e) {
            $('.loading').show();
            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);

            form.parsley().validate();

            if (form.parsley().isValid()) {

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        // $('form').trigger("reset");

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;

                    },
                    error: function (data) {
                    }
                });
            } else {
                $('.loading').hide();
            }
        });
    </script>

    {{--<script>--}}
    {{--CKEDITOR.replace('editor1');--}}
    {{--CKEDITOR.replace('editor2');--}}

    {{--</script>--}}

@endsection



