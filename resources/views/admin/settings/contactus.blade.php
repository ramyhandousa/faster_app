@extends('admin.layouts.master')

@section("title", __("maincp.call_us"))
@section('styles')

    <style>
        .customeStyleSocail{

            margin: 10px auto;

        }
    </style>
@endsection
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-0">
                    <div class="btn-group pull-right m-t-15">
                        <button type="button" class="btn btn-custom  waves-effect waves-light"
                                onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                        class="fa fa-reply"></i></span>
                        </button>
                    </div>

                </div>
                <h4 class="page-title">إعدادت عامة </h4>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive m-t-0">

                    <div class="form-group">

                        {{--<div class="col-lg-10 col-xs-12">--}}
                            {{--<label>عمولة التطبيق </label>--}}
                            {{--<input class="form-control number" type="number" style="margin: 15px auto" name="taxs"--}}
                                   {{--value="{{ $setting->getBody('taxs') }}" placeholder="العمولة"--}}
                                   {{--maxlength="500" >--}}
                        {{--</div>--}}

                        <div class="col-lg-5 col-xs-12">
                            <label>عمولة التطبيق </label>
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2">  <i class="fa fa-percent" aria-hidden="true"></i></span>
                                <input class="form-control percent_app" type="text"  id="whereEntry"  name="percent_app"
                                       value="{{ $setting->getBody('percent_app') }}" placeholder="عمولة التطبيق"
                                       pattern='\d+'
                                       data-parsley-pattern='\d+'
                                       maxlength="3" max="100" >
                            </div>
                        </div>
                        <div class="col-lg-5 col-xs-12">
                            <label>الحد الاقصي لعدد الطلبات مجانا </label>
                            <input class="form-control number" type="number" style="margin: 15px auto" name="max_order"
                                   value="{{ $setting->getBody('max_order') }}" placeholder="الحد الاقصي لعدد الطلبات مجانا"
                                   maxlength="3" >
                        </div>
                        
                        
                        <div class="col-lg-5 col-xs-12">
                            <label>الحد  الأدني لشحن المحفظة      </label>
                            <input class="form-control number" type="number" style="margin: 15px auto" name="min_wallet"
                                   value="{{ $setting->getBody('min_wallet') }}" placeholder="         الحد  الأدني لشحن المحفظة  "
                                   maxlength="3" >
                        </div>



                        <div class="col-lg-5 col-xs-12">
                            <label>@lang('maincp.unified_number') </label>
                            <input class="form-control" type="text" name="contactus_phone"
                                   value="{{ $setting->getBody('contactus_phone') }}" placeholder="0123456789"
                                   maxlength="500" >
                        </div>

                        <div class="col-lg-5 col-xs-12">
                            <label>@lang('maincp.e_mail') </label>
                            <input class="form-control" type="email" name="contactus_email"
                                   value="{{ $setting->getBody('contactus_email') }}" placeholder="Example@Advertisement.sa"
                                   maxlength="500"
                                   >
                        </div>

                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-facebook"></i></span>
                                <input type="text" class="form-control" name="contactus_facebook"
                                       value="{{ $setting->getBody('contactus_facebook') }}"
                                       placeholder="@lang('maincp.facebook') "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500" >
                            </div>
                        </div>

                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-twitter"></i></span>
                                <input type="text" name="contactus_twitter"
                                       value="{{ $setting->getBody('contactus_twitter') }}" class="form-control"
                                       placeholder="@lang('maincp.twitter') "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500" >
                            </div>
                        </div>

                        <!--<div class="col-lg-5 col-xs-12">-->
                        <!--    <div class="input-group customeStyleSocail">-->
                        <!--        <span class="input-group-addon" id="basic-addon2"><i class="fa fa-google-plus"></i></span>-->
                        <!--        <input type="text" name="contactus_google"-->
                        <!--               value="{{ $setting->getBody('contactus_google') }}" class="form-control"-->
                        <!--               placeholder=" "-->
                        <!--               aria-label="Recipient's username" aria- describedby="basic-addon2"-->
                        <!--               maxlength="500" >-->
                        <!--    </div>-->
                        <!--</div>-->

                        <!--<div class="col-lg-5 col-xs-12">-->
                        <!--    <div class="input-group customeStyleSocail">-->
                        <!--        <span class="input-group-addon" id="basic-addon2"><i class="fa fa-linkedin"></i></span>-->
                        <!--        <input type="text" name="contactus_linkedin"-->
                        <!--               value="{{ $setting->getBody('contactus_linkedin') }}" class="form-control"-->
                        <!--               placeholder=" "-->
                        <!--               aria-label="Recipient's username" aria- describedby="basic-addon2"-->
                        <!--               maxlength="500" >-->
                        <!--    </div>-->
                        <!--</div>-->

                        {{--<div class="col-lg-5 col-xs-12">--}}
                            {{--<div class="input-group">--}}
                                {{--<span class="input-group-addon" id="basic-addon2"><i class="fa fa-pinterest"></i></span>--}}
                                {{--<input type="text" name="contactus_pinterest"--}}
                                       {{--value="{{ $setting->getBody('contactus_pinterest') }}" class="form-control"--}}
                                       {{--placeholder=" "--}}
                                       {{--aria-label="Recipient's username" aria- describedby="basic-addon2"--}}
                                       {{--maxlength="500">--}}
                            {{--</div>--}}
                        {{--</div>--}}



                        <!--<div class="col-lg-5 col-xs-12">-->
                        <!--    <div class="input-group customeStyleSocail">-->
                        <!--        <span class="input-group-addon" id="basic-addon2"><i class="fa fa-snapchat"></i></span>-->
                        <!--        <input type="text" name="contactus_snapchat"-->
                        <!--               value="{{ $setting->getBody('contactus_snapchat') }}" class="form-control"-->
                        <!--               placeholder=" "-->
                        <!--               aria-label="Recipient's username" aria- describedby="basic-addon2"-->
                        <!--               maxlength="500">-->
                        <!--    </div>-->
                        <!--</div>-->




                        <div class="col-lg-5 col-xs-12">
                            <div class="input-group customeStyleSocail">
                                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-instagram"></i></span>
                                <input type="text" name="contactus_instagram"
                                       value="{{ $setting->getBody('contactus_instagram') }}" class="form-control"
                                       placeholder="@lang('maincp.instagram')  "
                                       aria-label="Recipient's username" aria- describedby="basic-addon2"
                                       maxlength="500">
                            </div>
                        </div>


                        <div class="col-xs-12 text-right">

                            <button type="submit" class="btn btn-warning">
                               @lang('maincp.save_data')   <i style="display: none;" id="spinnerDiv"
                                                class="fa fa-spinner fa-spin"></i>
                            </button>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            $('#spinnerDiv').show();

            var form = $(this);
            form.parsley().validate();
            if (form.parsley().isValid()) {
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {


                        if (data.status == true) {
                            //  $('#messageError').html(data.message);
                            $('#spinnerDiv').hide();
                            var shortCutFunction = 'success';
                            var msg = data.message;
                            var title = 'نجاح';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;
                        }

                        {{--setTimeout(function () {--}}
                        {{--window.location.href = '{{ route('categories.index') }}';--}}
                        {{--}, 3000);--}}
                    },
                    error: function (data) {
                    }
                });
            }
        });

    </script>
@endsection







