@extends('admin.layouts.master')
@section('title', __('trans.banks_management'))


@section('styles')



@endsection
@section('content')



    <form method="POST" action="{{ route('banks.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    {{--  <a href="{{ route('users.create') }}" type="button" class="btn btn-custom waves-effect waves-light"
                        aria-expanded="false"> @lang('maincp.add')
                         <span class="m-l-5">
                         <i class="fa fa-plus"></i>
                     </span>
                     </a> --}}
                </div>
                <h4 class="page-title">@lang('trans.add_bank')</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30">@lang('trans.add_bank')</h4>

                    <div class="row">
                        <div class="col-xs-8">

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">@lang('trans.bank_name')*</label>
                                <input type="text" name="name"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="@lang('trans.bank_name')..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
                                <label for="usernames">@lang('trans.account_number') *</label>
                                <input type="text" name="account_number" value="{{ old('account_number') }}"
                                       class="form-control requiredFieldWithMaxLenght" required placeholder="@lang('trans.account_number') ..."/>
                                @if($errors->has('account_number'))
                                    <p class="help-block">
                                        {{ $errors->first('account_number') }}
                                    </p>
                                @endif
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="form-group{{ $errors->has('iban_number') ? ' has-error' : '' }}">
                                <label for="usernames">@lang('trans.iban_number') *</label>
                                <input type="text" name="iban_number" value="{{ old('iban_number') }}"
                                       class="form-control requiredFieldWithMaxLenght" required placeholder="@lang('trans.iban_number') ..."/>
                                @if($errors->has('iban_number'))
                                    <p class="help-block">
                                        {{ $errors->first('iban_number') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="usernames">@lang('trans.bank_image') *</label>
                                <input type="file" name="image" class="dropify" data-max-file-size="6M"/>
                            </div>

                        </div>
                        <!-- end col -->

                        <div class="form-group text-right m-t-20">
                            <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                                @lang('maincp.save_data')
                            </button>
                            <button onclick="window.history.back();return false;" type="reset"
                                    class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                                @lang('maincp.disable')
                            </button>
                        </div>
                    </div>
                    </div>




                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        $('form').on('submit', function (e) {
            
            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);

            form.parsley().validate();

            if (form.parsley().isValid()) {
            $('.loading').show();
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        // $('form').trigger("reset");

                        var shortCutFunction = 'success';
                        var msg = data.message;
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                    }
                });
            } else {
                $('.loading').hide();
            }
        });

    </script>
@endsection

