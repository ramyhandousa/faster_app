@extends('admin.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title">قائمة تقارير االطلبات </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">

                <div class="row">

                </div>
                <br>

                <table id="datatable-fixed-header" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>رقم الطلب</th>
                        <th>اسم مزود الخدمة</th>
                        <th>حالة الطلب</th>
                        <th>مشاهدة تفاصيل الطلب</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>

                            <td style="width:10px"> {{$report->id}}  </td>
                            <td  >  {{optional( $report->provider)->name}} </td> 
                            <td>  {{  $report->status_append}} </td>

                            <td>
                                 <a href="{{ route('reports.show',$report->id) }}" 
                                   class="btn btn-icon btn-xs waves-effect btn-default m-b-5"> 
                                     التفاصيل 
                                 </a> 
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>
        function accept(fff) {
            var url = $(fff).attr('data-href');
            swal({
                title: "هل انت متأكد من التفعيل ؟",
                text: "سوف تكتب رسالة لسبب التفعيل.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#0edd35",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-success waves-effect waves-light',
                closeOnConfirm: false,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    window.location.href = url
                }
            });
        }

        function suspend(fff) {
            var url = $(fff).attr('data-href');
            swal({
                title: "هل تريد تعطيل المستخدم ؟",
                text: "سوف تكتب رسالة لبيان سبب التعطيل.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: false,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    window.location.href = url
                }
            });
        }

    function Delete(fff) {
    var url = $(fff).attr('data-href');
    swal({
    title: "هل انت متأكد من حذفه؟",
    text: "سوف تكتب رسالة لبيان سبب الحذف.",
    type: "error",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "موافق",
    cancelButtonText: "إلغاء",
    confirmButtonClass: 'btn-danger waves-effect waves-light',
    closeOnConfirm: false,
    closeOnCancel: true,
    }, function (isConfirm) {
    if (isConfirm) {
    window.location.href = url
    }
    });
    }


    @if(session()->has('errors'))
    setTimeout(function () {
        showErrors('{{ session()->get('errors') }}');
    }, 1000);

    @endif

    function showErrors(message) {

        var shortCutFunction = 'error';
        var msg = message;
        var title = 'فشل!';
        toastr.options = {
            positionClass: 'toast-top-center',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };

        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;

    }


    </script>




@endsection

