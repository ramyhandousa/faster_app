@extends('admin.layouts.master')
@section('title', 'الصفحة الرئيسية')


@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('maincp.control_panel')</h4>
        </div>
    </div>



        <div class="row statistics">

            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> عدد مزودي الخدمات</h4>

                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0">  {{$providersCount}}     </h2>
                                <p class="text-muted m-b-0">عدد مزودي الخدمات</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- end col -->
            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> عدد السائقين</h4>

                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0"> {{$driversCount}}  </h2>
                                <p class="text-muted m-b-0"> عدد السائقين</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- end col -->


            <!--<div class="col-lg-3 col-md-6">-->
            <!--    <a href="javascript:;">-->
            <!--        <div class="card-box">-->
            <!--            <h4 class="header-title m-t-0 m-b-30"> عدد   المساعدين لإدارة التطبيق</h4>-->

            <!--            <div class="widget-box-2">-->
            <!--                <div class="widget-detail-2">-->
            <!--                        <span class="pull-left"> <i-->
            <!--                                    class="zmdi zmdi-accounts zmdi-hc-3x"></i> </span>-->
            <!--                    <h2 class="m-b-0">  {{$userHelpAdminCount}} </h2>-->
            <!--                    <p class="text-muted m-b-0">  عدد المساعدين</p>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->
            <!--    </a>-->
            <!--</div>-->





            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> عدد المدن</h4>

                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-group-work zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0"> {{$citiesCount}} </h2>
                                <p class="text-muted m-b-0"> عدد المدن</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- end col -->


            <div class="col-lg-3 col-md-6">
                <a href="javascript:;">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">عدد رسائل تواصل معنا</h4>
                        <div class="widget-box-2">
                            <div class="widget-detail-2">
                                    <span class="pull-left"> <i
                                                class="zmdi zmdi-eye-off zmdi-hc-3x"></i> </span>
                                <h2 class="m-b-0">{{$messageNotReadCount}}  </h2>
                                <p class="text-muted m-b-0">عدد رسائل</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- end col -->
        </div>

@endsection
