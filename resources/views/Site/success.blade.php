<!DOCTYPE html>
<html lang="ar">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Title-->
    <title>اسرع</title>

    <!--Fonts -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <!-- materialize -->
     <link rel="stylesheet" href="{{ request()->root() }}/public/Site/css/materialize.min.css">
    <link rel="stylesheet" href="{{ request()->root() }}/public/Site/css/materialize-rtl.css">
    <link rel="stylesheet" href="{{ request()->root() }}/public/Site/css/style-ar.css">
</head>

<body>
<div class="container">
    <div class="row ">
        <div class="col s12 l12  text-center logo">
            <img src="{{ request()->root() }}/public/Site/images/logo.png" class="img-fluid">
            <h5>تهانينا</h5>
        </div>

        <div class="col s12 l12  text-center logo">
            <h5>تم إرسال طلبك بنجاح</h5>
        </div>


        <div style="margin-right: 40%">
            <img   src="{{ request()->root() }}/public/Site/images/ok.gif">
        </div>


    </div>
</div>

<!--scripts -->
<script src="{{ request()->root() }}/public/Site/js/jquery-3.2.1.min.js"></script>
<script src="{{ request()->root() }}/public/Site/js/materialize.min.js"></script>
 <script src="{{ request()->root() }}/public/Site/js/scripts.js"></script>


 </body>

</html>