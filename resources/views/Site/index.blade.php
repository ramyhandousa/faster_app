<!DOCTYPE html>
<html lang="ar">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Meta-->
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Title-->
    <title>اسرع</title>

    <!--Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tajawal">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- materialize -->
    <link rel="stylesheet" href="{{ request()->root()  }}/public/assets/admin/plugins/toastr/toastr.css" type="text/css" />
    <link rel="stylesheet" href="{{ request()->root() }}/public/Site/css/materialize.min.css">
    <link rel="stylesheet" href="{{ request()->root() }}/public/Site/css/materialize-rtl.css">
    <link rel="stylesheet" href="{{ request()->root() }}/public/Site/css/style-ar.css">
    <style>
        .help-block {
            color: #e74c3c !important;
            font-size: 16px !important;
            position: absolute;
            left: 15px;
            top: 50%;
            transform: translateY(-50%);

        }
        
        .pac-container {
            z-index: 1051 !important;
        }

    </style>
</head>

<body>
<div class="container">
    <div class="row ">
        <div class="col s12 l12  text-center logo">
            <img src="{{ request()->root() }}/public/Site/images/logo.png" class="img-fluid">
            <h5>اضف بيانات الطلب الخاص بك</h5>
        </div>
        
        <form class="col s12 l8 push-l2 " method="post" action="{{route('site.registerSite')}}"   >
            @csrf
            <input type="hidden" name="idUser" value="{{request()->provider}}">
            <div class="input-field">
                <input id="user_name"  value="{{ old('name') }}" name="name" type="text" class="validate" required >
                
                <label for="user_name">اسم المستخدم</label>
            </div>


            <div class="input-field">
                <input id="email" type="email" value="{{ old('email') }}" name="email" class="validate"  >
                <label for="email">البريد الالكتروني</label>
            </div>


            <div class="input-field">
                <input id="phone" type="tel" value="{{ old('phone') }}" name="phone" class="validate" minlength=10 maxlength="10"   required >
                <label for="phone">رقم الجوال</label>
               
            </div>



            <div class="input-field">
                <select name="city" id="myCity" class="validate"  >
                    <option value=""  >المدينة</option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>
            

            </div>
            <div class="input-field modal-trigger" href="#mapModal">
                <input id="map" type="text" required>
                 
                <input type="hidden" name="latitute" id="lat"/>
                <input type="hidden" name="longitute" id="lng"/>
                <input type="hidden" name="address" id="address"/>
                <label for="map">الموقع على الخريطة</label>
                <img src="{{ request()->root() }}/public/Site/images/map.png" class="img-fluid loc-img ">
            </div>
            <!-- map modal -->
            <div id="mapModal" class="modal modal-lg">
                <div class="modal-content">
                    <div class="row">
                        <!--<div class="col-md-8">-->
                             <input id="pac-input" name="address_search"
                                                               class="controls"
                                                               type="text"
                                                               style="z-index: 1;position: absolute;top: 11px;left: 113px;height: 34px;width: 63%;"
                                                               placeholder="@lang('institutioncp.search_address')">
                        <!--</div>-->
                        <!--<div class="col-md-4">-->
                            <a href="#" style="position: absolute;right: 500px;  top: 31px;font-size: 20px;color: red;z-index: 2;"  id="getLocation">               
                                <i class="fa fa-location-arrow"></i>
                                <!--<i class="far fa-location"></i>-->
                           </a>
                        <!--</div>-->
                    </div>
                    
                                                               
                    <div id="googleMap" style="height: 500px"></div>
                   
                </div>
            </div>

            <!-- end map modal -->
            <div class="input-field">
                <input  type="text"  id="user_address" value="{{ old('user_address') }}" name="userAddress"  class="validate" required>
                <label for="user_address">تفاصيل العنوان</label>
            </div>
            <div class="input-field">
                <input id="request" type="text" name="description" value="{{ old('description') }}" class="validate" required>
                <label for="request">تفاصيل الطلب</label>
            </div>
            <div class="input-field text-center">
                <button type="submit" id="submitForm" class="waves-effect waves-light btn submit-btn ">تأكيـــد</button>
            </div>

        </form>

    </div>
</div>

<!--scripts -->
<script src="{{ request()->root() }}/public/Site/js/jquery-3.2.1.min.js"></script>
<script src="{{ request()->root() }}/public/Site/js/materialize.min.js"></script>
<script src="{{ request()->root()  }}/public/assets/admin/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<script src="{{ request()->root() }}/public/Site/js/scripts.js"></script>
<script> 
    var map;
    window.lat = 0;
    window.lng = 0; 
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(updatePosition); 
        }
        return null;
    };
    function updatePosition(position) {
        
        if (position) {  
            window.lat = position.coords.latitude;
            window.lng = position.coords.longitude; 
             
            
            if(map.getCenter().lat() != position.coords.latitude && map.getCenter().lat() != position.coords.longitude ){
                console.log('updated');
            //  map.setCenter({lat:lat, lng:lng, alt:0});
            }
                // google.maps.event.addDomListener(window, 'load', initAutocomplete());
          
        }
      
    }
    setInterval(function(){updatePosition(getLocation());}, 1000);
    function currentLocation() {
        
      return {lat:window.lat, lng:window.lng};
    };
    
    $('#getLocation').click(function(){ 
        return map.setCenter({lat: lat != 0 ? lat : 30.06263 , lng:lng != 0 ? lng : 31.24967, alt:0});
    });
   
    @if(session()->has('errors'))
        setTimeout(function () {
        showErrors('{{ session()->get('errors') }}');
    }, 1000);

    @endif

    function showErrors(message) {

        var shortCutFunction = 'error';
        var msg = message;
        var title = 'فشل!';
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;


    }
    
    $('#submitForm').click(function () {
        var user_name = $('#user_name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        
         var map = $('#lat').val();
         var myCity = $('#myCity option:selected').val(); 
          
 
         
         if(user_name != '' &&email != '' &&phone != '' ){
             
             if (myCity == '' || myCity === undefined || myCity === null) {
                showErrors('من فضلك إختيار المدينة الخاصه بك ');
                return false;
            }
            
            if (map == '' || map === undefined || map === null) {
              showErrors('من فضلك إختيار الموقع الخاص بك ');
                return false;
            }
         }
         
        
       
    });


    function initAutocomplete() {

          map = new google.maps.Map(document.getElementById('googleMap'), {
            
                // center: {lat:  window.lat   , lng:  window.lng   },
            center: {lat: 30.06263, lng: 31.24967},
            zoom: 15,
            mapTypeId: 'roadmap'
        });


        var marker;
        google.maps.event.addListener(map, 'click', function (event) {

            map.setZoom();
            var mylocation = event.latLng;
            map.setCenter(mylocation);


            $('#lat').val(event.latLng.lat());
            $('#lng').val(event.latLng.lng());
             


            codeLatLng(event.latLng.lat(), event.latLng.lng());

            setTimeout(function () {
                if (!marker)
                    marker = new google.maps.Marker({position: mylocation, map: map});
                else
                    marker.setPosition(mylocation);
            }, 600);

        });


        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });


        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            // var location = place.geometry.location;
            // var lat = location.lat();
            // var lng = location.lng();
            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];


            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
                $('#lat').val(place.geometry.location.lat());
                $('#lng').val(place.geometry.location.lng());
                $('#address').val(place.formatted_address);


            });


            map.fitBounds(bounds);
        });


    }
    

    function showPosition(position) {

        map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});
        codeLatLng(position.coords.latitude, position.coords.longitude);


    }


    function codeLatLng(lat, lng) {

        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({
            'latLng': latlng
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    // console.log(results[1].formatted_address);
                    $("#demo").html(results[1].formatted_address);

                    $("#address").val(results[1].formatted_address);
                    $("#map").val(results[1].formatted_address);
                    $("#pac-input").val(results[1].formatted_address);
                    

                } else {
                }
            } else {
                alert('Geocoder failed due to: ' + status);
            }
        });
    }
    
    
 </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOHMIfoX5CvzkfHlfeuJKZEn2EZfKZ6qc&language={{ config('app.locale') }}&&callback=initAutocomplete&libraries=places"></script>

<script type="text/javascript">
    
    // google.maps.event.addDomListener(window, 'load',function(){
    //     console.log('Changed');
    //     // initAutocomplete()
    // } );
  
</script>
</body>

</html>