<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->insert([
//            'defined_user' =>  'delivery',
//            'phone' =>  Str::random(20),
//            'name' => Str::random(10),
//            'email' => Str::random(10).'@gmail.com',
//            'password' => bcrypt('secret'),
//            'city_id' => 1,
//            'api_token' => str_random(60),
//            'is_active' => 1,
//        ]);

        $count = 50;
        factory(\App\User::class, $count)->create();
    }
}
