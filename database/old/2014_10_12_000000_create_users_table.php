<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('defined_user', ['provider', 'delivery', 'other'])->default('other');
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('image')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('latitute')->nullable();
            $table->string('longitute')->nullable();
            $table->string('address')->nullable();
            $table->enum('action_provider', ['automatic', 'manual', 'other'])->default('other');
            $table->enum('setting_provider', ['public', 'private', 'other'])->default('other');
            $table->string('uuid');
            $table->string('api_token');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
