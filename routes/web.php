<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/test/payment', function(){
    
    $token = "7Fs7eBv21F5xAocdPvvJ-sCqEyNHq4cygJrQUFvFiWEexBUPs4AkeLQxH4pzsUrY3Rays7GVA6SojFCz2DMLXSJVqk8NG-plK-cZJetwWjgwLPub_9tQQohWLgJ0q2invJ5C5Imt2ket_-JAlBYLLcnqp_WmOfZkBEWuURsBVirpNQecvpedgeCx4VaFae4qWDI_uKRV1829KCBEH84u6LYUxh8W_BYqkzXJYt99OlHTXHegd91PLT-tawBwuIly46nwbAs5Nt7HFOozxkyPp8BW9URlQW1fE4R_40BXzEuVkzK3WAOdpR92IkV94K_rDZCPltGSvWXtqJbnCpUB6iUIn1V-Ki15FAwh_nsfSmt_NQZ3rQuvyQ9B3yLCQ1ZO_MGSYDYVO26dyXbElspKxQwuNRot9hi3FIbXylV3iN40-nCPH4YQzKjo5p_fuaKhvRh7H8oFjRXtPtLQQUIDxk-jMbOp7gXIsdz02DrCfQIihT4evZuWA6YShl6g8fnAqCy8qRBf_eLDnA9w-nBh4Bq53b1kdhnExz0CMyUjQ43UO3uhMkBomJTXbmfAAHP8dZZao6W8a34OktNQmPTbOHXrtxf6DS-oKOu3l79uX_ihbL8ELT40VjIW3MJeZ_-auCPOjpE3Ax4dzUkSDLCljitmzMagH2X8jN8-AYLl46KcfkBV"; #token value to be placed here;

        $basURL = "https://apitest.myfatoorah.com";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$basURL/v2/InitiatePayment",
            CURLOPT_CUSTOMREQUEST => "POST",
            
          CURLOPT_POSTFIELDS => "{\"InvoiceAmount\":0,\"CurrencyIso\": \"SAR\"}",
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);




        $results =  json_decode($response, true);

        if($results['IsSuccess']){
            
            $methods = collect($results['Data']['PaymentMethods'])->whereIn('PaymentMethodId', [2,6])->values()->all();
            
            return response()->json(['status' => 200, 'data' => $methods ]);
        }
});






Auth::routes();



Route::get('/', 'HomeController@index')->name('home')->middleware('validWebSite');

Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');
    
    Route::group(['prefix' => 'administrator'], function () {


        Route::get('/login', 'Admin\LoginController@login')->name('admin.login');
        Route::post('/login', 'Admin\LoginController@postLogin')->name('admin.postLogin');
        
        // Password Reset Routes...
        
        Route::get('password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
        Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
        Route::get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
        Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset');
        
    });
    
    Route::group(['prefix' => 'administrator', 'middleware' => ['admin']], function () {
        
        Route::get('/', 'Admin\HomeController@index')->name('home');
        Route::get('/home', 'Admin\HomeController@index')->name('admin.home');
    
    
        // administrators --- Admin Panel users
        // add admin or some one who controll Or help for admin panel
    
        Route::resource('helpAdmin', 'Admin\HelpAdminController');
        Route::get('helpAdmin/{id}/delete', 'Admin\HelpAdminController@delete')->name('helpAdmin.delete');
        Route::post('helpAdmin/{id}/delete', 'Admin\HelpAdminController@deleteHelpAdmin')->name('helpAdmin.message.delete');
        Route::post('helpAdmin/{id}/suspend', 'Admin\HelpAdminController@suspendHelpAdmin')->name('helpAdmin.message.suspend');
        Route::get('user/{id}/delete', 'Admin\UsersController@delete')->name('user.for.delete');
        Route::post('user/suspend', 'Admin\HelpAdminController@suspend')->name('user.suspend');
       Route::post('users/acceptedOrRefuse', 'Admin\UsersController@acceptedOrRefuse')->name('providers.acceptedOrRefuse');

        // Roles routes ..........
        Route::resource('roles', 'Admin\RolesController');
        Route::post('role/delete', 'Admin\RolesController@delete')->name('role.delete');
    
    
        Route::resource('users', 'Admin\UsersController');
        Route::post('accepted','Admin\UsersController@accpetedUser')->name('user.accepted');
    
        Route::get('settings/aboutus', 'Admin\SettingsController@aboutus')->name('settings.aboutus');
        Route::get('settings/order', 'Admin\SettingsController@order')->name('settings.order');
        Route::get('settings/taxs', 'Admin\SettingsController@taxs')->name('settings.taxs');
        Route::get('settings/terms', 'Admin\SettingsController@terms')->name('settings.terms');
        Route::get('settings/suspendElement', 'Admin\SettingsController@suspendElement')->name('settings.suspendElement');

//        Route::resource('faqs', 'Admin\FaqsController');

        Route::get('/settings/app-general-settings', 'Admin\SettingsController@appGeneralSettings')->name('settings.app.general');
        Route::get('settings/contacts', 'Admin\SettingsController@contactus')->name('settings.contactus');


        Route::post('/settings', 'Admin\SettingsController@store')->name('administrator.settings.store');
        Route::post('/packageStore', 'Admin\SettingsController@packageStore')->name('administrator.package.store');

        Route::post('contactus/reply/{id}', 'Admin\SupportsController@reply')->name('support.reply');
        Route::get('contactus', 'Admin\SupportsController@index')->name('support.index');
        Route::get('contactus/{id}', 'Admin\SupportsController@show')->name('support.show');
        Route::post('support/contact/delete', 'Admin\SupportsController@delete')->name('support.contact.delete');


        Route::resource('supports', 'Admin\SupportsController');
        Route::post('supports/delete', 'Admin\SupportsController@delete')->name('supports.delete');

         Route::resource('types', 'Admin\TypesSupportController');


        Route::post('city/delete/group', 'Admin\CitiesController@groupDelete')->name('cities.group.delete');
        Route::post('cities/delete', 'Admin\CitiesController@delete')->name('city.delete');
        Route::resource('cities', 'Admin\CitiesController');
        Route::post('city/suspend', 'Admin\CitiesController@suspend')->name('city.suspend');


        Route::resource('banks', 'Admin\BanksController');
        Route::post('bank/suspend', 'Admin\BanksController@suspend')->name('bank.suspend');


        Route::resource('reports', 'Admin\ReportsController');
        
        Route::post('/logout', 'Admin\LoginController@logout')->name('administrator.logout');
        
    });



Route::get('/sub', function (Illuminate\Http\Request $request) {

    $cities =\App\Models\City::whereParentId($request->id)->get();

    if (!empty($cities) && count($cities) > 0){
        return response()->json( $cities);
    }else{

        return response()->json(401);
    }


})->name('getSub');



Route::group([
    'namespace' => 'Site',
    'middleware' => 'localeControlPanel',
], function () {


    Route::get('/','indexController@index')->name('firstPageSite');
    Route::get('/success','indexController@success')->name('successRegister');
    Route::post('/registerAccount','indexController@register')->name('site.registerSite');

});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

 

