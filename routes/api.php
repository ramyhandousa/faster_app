<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group([
    'middleware' =>  'cors',
    'namespace' => 'Api',
], function () {

    Route::prefix('Auth')->group(function () {

        Route::post('login', 'LoginController@login');
        Route::post('register', 'RegisterController@register');
        Route::post('forgetPassword', 'ForgotPasswordController@forgetPassword');
        Route::post('resetPassword', 'ForgotPasswordController@resetPassword');
        Route::post('checkCode', 'ResetPasswordController@checkCodeActivation');
        Route::post('resendCode', 'ResetPasswordController@resendCode');
        Route::post('changePassword', 'ResetPasswordController@changPassword');
        Route::post('editProfile', 'UsersController@editProfile')->middleware('apiToken');
        Route::post('logOut','LoginController@logOut');

    });
    
    
    
    Route::prefix('payments')->group(function () {

        Route::get('/get/methods','PaymentController@getPaymentMethods')->middleware('apiToken');
        Route::post('/excute','PaymentController@excutePayment')->middleware('apiToken');
        
        
        Route::get('/success','PaymentController@success');
        
        
        Route::get('/errors','PaymentController@errors');
        
       

    });
    
    Route::prefix('users')->group(function () {

        Route::get('userId','UsersController@getUser');
        Route::get('wallet','UsersController@wallet')->middleware('apiToken');
        Route::get('listProvider','UsersController@listProvider');
        Route::post('updateSetting','UsersController@updateSetting')->middleware('apiToken');
        Route::get('notification','UsersController@notification')->middleware('apiToken');
        Route::get('listFollowing','UsersController@listFollowing')->middleware('apiToken');
        Route::get('listFollowers','UsersController@listFollowers')->middleware('apiToken');
        Route::post('followOrUnFollow','UsersController@followOrUnFollowUser')->middleware('apiToken');

    });

    Route::prefix('cities')->group(function () {

        Route::get('/','CityController@getCity');


    });

    Route::prefix('orders')->group(function () {

        Route::get('listWaiting','ListOrderController@listWaiting');
        Route::get('listPending','ListOrderController@listPending')->middleware('apiToken');
        Route::get('listAccepted','ListOrderController@listAccepted');
        Route::get('listFinish','ListOrderController@listFinish');
        Route::get('show','ListOrderController@showMasterOrder');
        Route::get('showOrderByProvider','ListOrderController@showOrder');
        
        Route::post('store','OrderController@storeOrder')->middleware('apiToken');

        Route::post('providerAccepted' , 'MasterOrderController@providerAcceptedOrder')->middleware(['checkAcceptOrderByProvider','orderProcessBefore']);
        Route::post('ProviderRefuse' , 'MasterOrderController@refuseOrderByProvider')->middleware('refuseOrderByProvider');
        Route::post('pendingOrder' , 'MasterOrderController@pendingOrder')->middleware('processOrder');

        Route::post('deliverAccepted' , 'MasterOrderController@acceptedDeliverOrder')->middleware('checkAcceptOrderByDelivery');
        Route::post('deliveryRefuse' , 'MasterOrderController@refuseDeliveryOrder')->middleware('refuseOrderByDelivery');
        Route::post('finishOrder' , 'MasterOrderController@finishOrderByDelivery')->middleware('validFinishOrder');

    });


    Route::prefix('setting')->group(function () {

        Route::get('listPriceOrder','SettingController@listPriceOrder');
        Route::get('aboutUs','SettingController@aboutUs');
        Route::get('terms','SettingController@terms');
        Route::get('getTypesSupport','SettingController@getTypesSupport');
        Route::post('contactUs','SettingController@contactUs');
        Route::post('send','SettingController@send');
        Route::get('searchCode','SettingController@searchCode');
        
        Route::get('banks','SettingController@listBanks');
    });

Route::post('testDistance' , 'MasterOrderController@testDistance');

Route::post('deleteNow',function (){


    $master = \App\Models\MasterOrder::all();

    $master->each->delete();

    \App\Models\DriverNotify::truncate();
    \App\Models\Notification::truncate();
    \App\Models\DriverNotify::truncate();
    $orders =\App\Models\Order::whereNotIn('id',[1,2,3,4])->get();
    $orders->each->delete();

});


});








